package maestro.my.music;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import maestro.my.music.ui.*;
import maestro.my.music.utils.PagerIndicator;
import maestro.svg.SVGInstance;

public class Main extends AppCompatActivity {

    public static final String TAG = Main.class.getSimpleName();

    public static final int MAIN = 0;
    public static final int ALBUMS = 1;
    public static final int MUSIC = 2;
    public static final int PLAYLISTS = 3;
    public static final int ARTISTS = 4;
    public static final int GENRES = 5;
    public static final int RADIO_CATEGORIES = 6;

    private static final String PARAM_PREV_FRAGMENT = "prev_fragment";

    private FragmentTransaction mTransaction;
    private DrawerLayout mDrawer;
    private Toolbar mToolbar;
    private PagerIndicator mIndicator;
    private ColorDrawable mToolbarBackground = new ColorDrawable();
    private boolean isDualPane;
    private int mCurrentId = -1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawer.setDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                commitTransaction();
            }
        });

        isDualPane = findViewById(R.id.play_fragment_frame) != null;

        if (savedInstanceState == null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, new MainFragment())
                    .replace(R.id.navigation_frame, new MNavigationFragment());
            if (isDualPane) {
                transaction.replace(R.id.play_fragment_frame, new PlayFragment());
            }
            transaction.commit();
        } else {
            mCurrentId = savedInstanceState.getInt(PARAM_PREV_FRAGMENT);
        }

        if (Settings.getInstance().isFirstStart()){
            new StartFragment().show(getSupportFragmentManager(), StartFragment.TAG);
        }

    }

    public void setSupportActionBar(Toolbar toolbar, PagerIndicator indicator) {
        mToolbar = toolbar;
        mIndicator = indicator;
        setSupportActionBar(toolbar);
    }

    @Override
    public void setSupportActionBar(Toolbar toolbar) {
        super.setSupportActionBar(toolbar);
        toolbar.setBackgroundDrawable(mToolbarBackground);
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View v = toolbar.getChildAt(i);
            if (v instanceof TextView) {
                ((TextView) v).setTextColor(Color.WHITE);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(PARAM_PREV_FRAGMENT, mCurrentId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem search = menu.findItem(R.id.search);
        search.setIcon(SVGInstance.getDrawable(R.raw.ic_search, Color.WHITE));
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (mDrawer.isDrawerOpen(Gravity.LEFT)) {
                mDrawer.closeDrawer(Gravity.LEFT);
            } else {
                mDrawer.openDrawer(Gravity.LEFT);
            }
            return true;
        } else if (item.getItemId() == R.id.search) {
            new NetworkSearchFragment().show(getSupportFragmentManager(), NetworkSearchFragment.TAG);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(Gravity.LEFT)) {
            mDrawer.closeDrawer(Gravity.LEFT);
        } else
            super.onBackPressed();
    }

    public void openFragment(int id) {
        if (id != mCurrentId) {
            mCurrentId = id;
            if (mTransaction == null) {
                mTransaction = getSupportFragmentManager().beginTransaction();
            }
            switch (id) {
                case MAIN:

                    break;
                case ALBUMS:
                    mTransaction.replace(R.id.content_frame, new AlbumFragment(), AlbumFragment.TAG);
                    break;
                case MUSIC:

                    break;
                case PLAYLISTS:

                    break;
                case ARTISTS:

                    break;
                case GENRES:

                    break;
                case RADIO_CATEGORIES:
                    mTransaction.replace(R.id.content_frame, new RadioMainFragment(), RadioMainFragment.TAG);
                    break;
            }

            if (!mDrawer.isDrawerOpen(Gravity.LEFT)) {
                commitTransaction();
            }

        }
    }

    private final void commitTransaction() {
        if (mTransaction != null) {
            mTransaction.commit();
            mTransaction = null;
        }
    }

    public void setToolbarColor(int color) {
        if (mToolbar != null && mIndicator != null) {
            mToolbarBackground.setColor(color);
            mIndicator.setBackgroundColor(color);
        }
    }

    public void openPlayFragment() {
        if (!isDualPane) {
            new PlayFragment().show(getSupportFragmentManager(), PlayFragment.TAG);
        }
    }
}
