package maestro.my.music.ui;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import com.dream.android.mim.MIM;
import com.dream.android.mim.MIMBlurMaker;
import com.dream.android.mim.MIMCirclePostMaker;
import com.dream.android.mim.RecyclingImageView;
import com.msoft.android.mplayer.lib.models.Song;
import maestro.my.music.R;
import maestro.my.music.Utils;
import maestro.my.music.data.IMyMusicService;
import maestro.my.music.data.MyMusicService;
import maestro.my.music.data.MyMusicServiceListener;
import maestro.my.music.data.MyMusicServiceUpdateListener;
import maestro.my.music.data.radio.RadioStation;
import maestro.svg.SVGInstance;

import java.util.Map;

/**
 * Created by Artyom on 7/6/2015.
 */
public class PlayFragment extends BaseFragment implements View.OnClickListener, OnSeekBarChangeListener {

    public static final String TAG = PlayFragment.class.getSimpleName();

    private static final MIMCirclePostMaker mCirclePostMaker = new MIMCirclePostMaker();
    private static final MIMBlurMaker mBlurMaker = new MIMBlurMaker();

    private MyMusicServiceListener mListener = new MyMusicServiceListener.Stub() {
        @Override
        public void onPlayStateChange(final int state) throws RemoteException {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        switch (MyMusicService.PLAY_STATE.values()[state]) {
                            case IDLE:

                                break;
                            case PLAYING:
                            case STOP:
                                ensurePlayButton();
                                break;
                            case SONG_CHANGE:
                                if (mBackgroundImage != null && getService() != null) {
                                    try {
                                        Object coverObject = null;
                                        if (getService().isPlayingSongs()) {
                                            Song song = getService().getCurrentPlayingSongs().get(getService().getCurrentPlayingPosition());
                                            coverObject = song;
                                            String key = Utils.getKey(coverObject);
                                            mPlayingCover.setLoadObject(MIM.by(Utils.MIM_IMAGE)
                                                    .to(mPlayingCover, key)
                                                    .object(coverObject)
                                                    .postMaker(mCirclePostMaker));
                                            mBackgroundImage.setLoadObject(MIM.by(Utils.MIM_IMAGE)
                                                    .to(mBackgroundImage, key + "_blur")
                                                    .object(coverObject)
                                                    .config(Bitmap.Config.ARGB_8888)
                                                    .postMaker(mBlurMaker));
                                            mTitle.setText(song.Title);
                                            mAuthor.setText(song.Author);
                                            mAlbum.setText(song.Album);
                                        } else {
                                            coverObject = getService().getCurrentRadioStation();
                                            String key = Utils.getKey(coverObject);
                                            String imageUrl = ((RadioStation) coverObject).getImageUrl();
                                            mPlayingCover.setLoadObject(MIM.by(Utils.MIM_NETWORK)
                                                    .to(mPlayingCover, key, imageUrl));
                                            mBackgroundImage.setLoadObject(MIM.by(Utils.MIM_NETWORK)
                                                    .to(mBackgroundImage, key + "_blur", imageUrl)
                                                    .config(Bitmap.Config.ARGB_8888)
                                                    .object(coverObject).postMaker(mBlurMaker));
                                        }
                                    } catch (RemoteException e) {
                                        e.printStackTrace();
                                    }
                                    ensurePlayButton();
                                }
                                break;
                        }
                    }
                });
            }
        }
    };

    private ImageView btnShuffle;
    private ImageView btnRepeat;
    private ImageView btnToggle;
    private ImageView btnNext;
    private ImageView btnPrevious;
    private ImageView btnMore;
    private TextView mTitle;
    private TextView mAuthor;
    private TextView mAlbum;
    private RecyclingImageView mPlayingCover;
    private RecyclingImageView mBackgroundImage;
    private WaveIndicator mWaveIndicator;
    private SeekBar mSeekBar;

    private boolean isProgressOnTouch;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.play_view, null);
        mTitle = (TextView) v.findViewById(R.id.title);
        mAuthor = (TextView) v.findViewById(R.id.author);
        mAlbum = (TextView) v.findViewById(R.id.album);
        mWaveIndicator = (WaveIndicator) v.findViewById(R.id.wave_indicator);
        mPlayingCover = (RecyclingImageView) v.findViewById(R.id.play_cover);
        mBackgroundImage = (RecyclingImageView) v.findViewById(R.id.background_image);
        btnShuffle = (ImageView) v.findViewById(R.id.shuffle);
        btnRepeat = (ImageView) v.findViewById(R.id.repeat);
        btnToggle = (ImageView) v.findViewById(R.id.toggle);
        btnNext = (ImageView) v.findViewById(R.id.next);
        btnPrevious = (ImageView) v.findViewById(R.id.previous);
        btnMore = (ImageView) v.findViewById(R.id.play_options);
        mSeekBar = (SeekBar) v.findViewById(R.id.play_progress);

        SVGInstance.applySVG(btnShuffle, R.raw.ic_shuffle, Color.WHITE);
        SVGInstance.applySVG(btnRepeat, R.raw.ic_repeat, Color.WHITE);
        SVGInstance.applySVG(btnNext, R.raw.ic_fast_forward, Color.WHITE, SVGInstance.DPI + 1.5f);
        SVGInstance.applySVG(btnPrevious, R.raw.ic_fast_rewind, Color.WHITE, SVGInstance.DPI + 1.5f);
        SVGInstance.applySVG(btnToggle, R.raw.ic_play, Color.WHITE, SVGInstance.DPI + 1.5f);
        SVGInstance.applySVG(btnMore, R.raw.ic_more, Color.WHITE);

        btnShuffle.setOnClickListener(this);
        btnRepeat.setOnClickListener(this);
        btnToggle.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        btnPrevious.setOnClickListener(this);
        btnMore.setOnClickListener(this);

        mSeekBar.setOnSeekBarChangeListener(this);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        ensurePlayButton();
        ensureShuffleAndLoopButtons();
    }

    @Override
    public void onServiceConnected(IMyMusicService service) {
        super.onServiceConnected(service);
        ensurePlayButton();
        try {
            service.attachUpdateListener(getTag(), mUpdateListener);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onServiceDisconnected(IMyMusicService service) {
        super.onServiceDisconnected(service);
        try {
            service.detachUpdateListener(getTag());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getKey() {
        return TAG;
    }

    @Override
    public MyMusicServiceListener getServiceListener() {
        return mListener;
    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.shuffle:
                    getService().setShuffle(!getService().isShuffle());
                    ensureShuffleAndLoopButtons();
                    break;
                case R.id.repeat:
                    final int loopState = getService().getLoopState() + 1;
                    getService().setLoopState(loopState > 2 ? 0 : loopState);
                    ensureShuffleAndLoopButtons();
                    break;
                case R.id.next:
                    getService().next();
                    break;
                case R.id.previous:
                    getService().prev();
                    break;
                case R.id.toggle:
                    if (getService() != null) {
                        getService().toggle();
                    }
                    break;
                case R.id.play_options:
                    //TODO: todo
                    break;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private final void ensurePlayButton() {
        if (getService() != null && getActivity() != null && btnToggle != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.e(TAG, "isPlaying: " + getService().isPlaying());
                        SVGInstance.applySVG(btnToggle, getService().isPlaying() ? R.raw.ic_pause : R.raw.ic_play, Color.WHITE, SVGInstance.DPI + 1.5f);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void ensureShuffleAndLoopButtons() {
        if (getService() != null && getActivity() != null && btnShuffle != null && btnRepeat != null) {
            try {
                btnShuffle.setAlpha(getService().isShuffle() ? 1f : 0.5f);
                final int loopState = getService().getLoopState();
                SVGInstance.applySVG(btnRepeat, getService().getLoopState() == 2 ? R.raw.ic_repeat_one : R.raw.ic_repeat, Color.WHITE);
                btnRepeat.setAlpha(loopState == 0 ? 0.5f : 1f);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    private final MyMusicServiceUpdateListener mUpdateListener = new MyMusicServiceUpdateListener.Stub() {

        @Override
        public void onPlayTimeUpdate(final int current, final int total) throws RemoteException {
            Log.e(TAG, "onPlayTimeUpdate: " + current + ", " + total);
            if (mSeekBar != null && !isProgressOnTouch) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSeekBar.setMax(total);
                        mSeekBar.setProgress(current);
                    }
                });
            }
        }

        @Override
        public void onRadioInfoUpdate(Map info) throws RemoteException {
            //TODO: apply info
        }
    };

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser && getService() != null) {
            try {
                getService().seekTo(progress);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        isProgressOnTouch = true;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        isProgressOnTouch = false;
    }

    @Override
    public boolean isFullscreenOnly() {
        return true;
    }

    @Override
    public boolean useSlideAnimation() {
        return true;
    }
}