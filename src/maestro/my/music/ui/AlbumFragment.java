package maestro.my.music.ui;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.Loader;
import android.support.v7.widget.RecyclerView;
import com.msoft.android.mplayer.lib.models.Album;
import maestro.my.music.R;
import maestro.my.music.data.Loders;
import maestro.my.music.data.UniqAdapter;
import maestro.my.music.utils.SpaceItemDecoration;

import java.util.ArrayList;

/**
 * Created by Artyom on 7/6/2015.
 */
public class AlbumFragment extends BaseContentFragment<Album> {

    public static final String TAG = AlbumFragment.class.getSimpleName();

    @Override
    public RecyclerView.ItemDecoration getDecoration() {
        return new SpaceItemDecoration(getSpanCount(), getResources().getDimensionPixelSize(R.dimen.item_spacing),
                getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material) * 2, false);
    }

    @Override
    public int getSpanCount() {
        return getResources().getInteger(R.integer.grid_columns);
    }

    @Override
    public int getLoaderId() {
        return TAG.hashCode();
    }

    @Override
    public UniqAdapter.TYPE getAdapterType() {
        return UniqAdapter.TYPE.GRID;
    }

    @Override
    public Loader<ArrayList<Album>> onCreateLoader(int i, Bundle bundle) {
        return new Loders.AlbumLoader(getActivity(), null);
    }

    @Override
    public void onItemClick(UniqAdapter.Holder holder, Object item) {
        super.onItemClick(holder, item);
        SongsFragment.makeInstance((Parcelable) item).show(getChildFragmentManager(), SongsFragment.TAG);
    }

    @Override
    public String getKey() {
        return TAG;
    }
}
