package maestro.my.music.ui;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by Artyom on 7/8/2015.
 */
public class TopsLayout extends ViewGroup {

    public static final String TAG = TopsLayout.class.getSimpleName();

    private int mSpace;
    private int mDefWidth;
    private int mDefHeight;
    private int mColumnCount;
    private int maxItems = 6;

    private ArrayList<Object> mObjects = new ArrayList<>();
    private ViewDrawer mDrawer = new DummyDrawer();

    public TopsLayout(Context context) {
        super(context);
        init();
    }

    public TopsLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TopsLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public TopsLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    void init() {
        mSpace = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
        mDefWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 156, getResources().getDisplayMetrics());
        mDefHeight = mDefWidth + (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 48, getResources().getDisplayMetrics());
        prepareItems();
    }

    public interface ViewDrawer<ItemType> {

        View getView(ItemType item);

    }

    public interface OnItemClickListener<ItemType> {

        void onItemClick(ItemType item);

    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        if (mDrawer != null && mObjects != null && mObjects.size() > 0) {
            final int childCount = getChildCount();

            int leftPos = getPaddingLeft() + mSpace;
            int topPos = getPaddingTop() + mSpace;

            for (int i = 0; i < childCount; i++) {
                final View child = getChildAt(i);
                final int width = child.getMeasuredWidth();
                final int height = child.getMeasuredHeight();

                child.layout(leftPos, topPos, leftPos + width, topPos + height);

                if ((i == 0 && mColumnCount == 1) || (i > 0 && (i - 1) % mColumnCount == 0)) {
                    leftPos = getPaddingLeft() + mSpace;
                    topPos += height + mSpace;
                } else {
                    leftPos += mSpace + width;
                }

            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        final int width = getMeasuredWidth();
        final int height = getMeasuredHeight();

        int outHeight = mSpace;

        if (mDrawer != null && mObjects != null && mObjects.size() > 0) {
            computeColumns(width);

            for (int i = 0; i < getChildCount(); i++) {
                View child = getChildAt(i);
                ViewGroup.LayoutParams mParams = child.getLayoutParams();
                child.measure(MeasureSpec.EXACTLY | mParams.width, MeasureSpec.EXACTLY | mParams.height);
                if (i % mColumnCount == 0) {
                    outHeight += child.getMeasuredHeight() + mSpace;
                }
            }

            if (outHeight != height) {
                setMeasuredDimension(width, outHeight);
            }

        }
    }

    @Override
    protected LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(mDefWidth, mDefHeight);
    }

    public int getDefWidth() {
        return mDefWidth;
    }

    public int getDefHeight() {
        return mDefHeight;
    }

    public void setDrawer(ViewDrawer drawer) {
        mDrawer = drawer;
    }

    public void addItems(ArrayList objects) {
        mObjects.clear();
        mObjects.addAll(objects);
        prepareItems();
    }

    private void prepareItems() {
        removeAllViews();
        if (mDrawer instanceof DummyDrawer) {
            mObjects.add(new Object());
            mObjects.add(new Object());
            mObjects.add(new Object());
        }

        if (mObjects != null && mObjects.size() > 0) {
            for (int i = 0; i < (maxItems < mObjects.size() ? maxItems : mObjects.size()); i++) {
                addView(mDrawer.getView(mObjects.get(i)));
            }
            requestLayout();
        }
    }

    private void computeColumns(int width) {
        mColumnCount = (width - mSpace * 2) / mDefWidth;
    }

    private final class DummyDrawer implements ViewDrawer<Object> {

        @Override
        public View getView(Object item) {

            int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 156, getResources().getDisplayMetrics());
            int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 48, getResources().getDisplayMetrics()) + width;

            View view = new View(getContext());
            view.setLayoutParams(new ViewGroup.LayoutParams(width, height));
            view.setBackgroundColor(getRandomColor());
            return view;
        }

        private int getRandomColor() {
            int R = (int) (Math.random() * 256);
            int G = (int) (Math.random() * 256);
            int B = (int) (Math.random() * 256);
            return Color.rgb(R, G, B);
        }

    }

}
