package maestro.my.music.ui;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.VideoView;
import maestro.my.music.R;

/**
 * Created by Artyom on 8/9/2015.
 */
public class StartFragment extends BaseFragment {

    public static final String TAG = StartFragment.class.getSimpleName();

    private VideoView mVideoView;
    private ViewPager mPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.start_fragment, null);
        mVideoView = (VideoView) v.findViewById(R.id.video_view);
        mPager = (ViewPager) v.findViewById(R.id.pager);
        mPager.setAdapter(new FragmentStatePagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int i) {
                return StartItemObject.makeInstance("This is test text or something else");
            }

            @Override
            public int getCount() {
                return 5;
            }
        });
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mVideoView.setVideoURI(Uri.parse("https://gdfr6w-sn3301.files.1drv.com/y3m1WV8_h3NaHV_1RDb6GO27ypL--_Khgcqrr65fwc0Rymo3djDc6fKyWpr8-Mi6XRVU5S7zXGUDvKDm0suzLb4TK_IlrUjrWsBwgK6N7nRTsfTqQjr8GLpX4mRooiFzDC9CWqIin4OXBrAK4y_ASThWA/boris1_CLIPCHAMP_720p.mp4?download&psid=1"));
        mVideoView.start();
        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mVideoView.start();
            }
        });
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
            }
        });
        mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.e(TAG, "onError: " + what);
                return false;
            }
        });
    }

    @Override
    public String getKey() {
        return TAG;
    }

    @Override
    public boolean isFullscreenOnly() {
        return true;
    }

    public static final class StartItemObject extends Fragment {

        private static final String PARAM_TEXT = "text";

        public static final StartItemObject makeInstance(String text) {
            StartItemObject itemObject = new StartItemObject();
            Bundle args = new Bundle(1);
            args.putString(PARAM_TEXT, text);
            itemObject.setArguments(args);
            return itemObject;
        }

        private TextView mText;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View v = inflater.inflate(R.layout.start_fragment_item_view, null);
            mText = (TextView) v.findViewById(R.id.text);
            mText.setText(getArguments().getString(PARAM_TEXT));
            return v;
        }
    }

}