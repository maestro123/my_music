package maestro.my.music.ui;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.app.DialogFragment;
import android.view.Window;
import com.dream.android.mim.MIMUtils;
import maestro.my.music.Main;
import maestro.my.music.MyMusicApplication;
import maestro.my.music.R;
import maestro.my.music.data.IMyMusicService;
import maestro.my.music.data.MyMusicServiceListener;
import maestro.my.music.utils.ThemeHolder;

/**
 * Created by Artyom on 7/6/2015.
 */
public abstract class BaseFragment extends DialogFragment implements MyMusicApplication.OnServiceConnectedListener {

    public static final String TAG = BaseFragment.class.getSimpleName();

    Main main;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isFullscreenOnly()
                || (isFullscreen() && !getResources().getBoolean(R.bool.is7inch)
                && !getResources().getBoolean(R.bool.is10inch))) {
            setStyle(STYLE_NORMAL, R.style.MyMusic_Theme_Overlay);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (useSlideAnimation()) {
            dialog.getWindow().setWindowAnimations(R.style.WindowSlideAnimation);
        }
        if (Build.VERSION.SDK_INT > 20) {
            Window window = dialog.getWindow();
            window.setStatusBarColor(MIMUtils.blendColors(getToolbarColor(), Color.BLACK, 0.7f));
        }
        return dialog;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof Main) {
            main = (Main) activity;
        }
        getApplication().attachOnServiceConnectedListener(this);
    }

    @Override
    public void onDetach() {
        main = null;
        getApplication().detachOnServiceConnectedListener(this);
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (main != null) {
            main.setToolbarColor(getToolbarColor());

            if (Build.VERSION.SDK_INT > 20) {
                Window window = main.getWindow();
                window.setStatusBarColor(MIMUtils.blendColors(getToolbarColor(), Color.BLACK, 0.7f));
            }

        }
    }

    //TODO: very very bad thing
    public void reattachServiceListener() {
        getApplication().attachOnServiceConnectedListener(this);
    }

    public IMyMusicService getService() {
        return ((MyMusicApplication) getActivity().getApplication()).getService();
    }

    public MyMusicApplication getApplication() {
        return (MyMusicApplication) getActivity().getApplication();
    }

    public int getToolbarColor() {
        return ThemeHolder.getInstance().getToolbarColor();
    }

    public boolean useSlideAnimation() {
        return false;
    }

    public boolean isFullscreen() {
        return false;
    }

    public boolean isFullscreenOnly() {
        return false;
    }

    public MyMusicServiceListener getServiceListener() {
        return null;
    }

    @Override
    public void onServiceConnected(IMyMusicService service) {
        try {
            MyMusicServiceListener listener = getServiceListener();
            if (listener != null) {
                service.attachListener(getKey(), getServiceListener());
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onServiceDisconnected(IMyMusicService service) {
        try {
            service.detachListener(getKey());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public abstract String getKey();

}
