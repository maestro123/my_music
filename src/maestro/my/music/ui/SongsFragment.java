package maestro.my.music.ui;

import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.RemoteException;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.dream.android.mim.MIM;
import com.dream.android.mim.MIMAbstractDisplayer;
import com.dream.android.mim.RecyclingImageView;
import com.msoft.android.mplayer.lib.models.Album;
import com.msoft.android.mplayer.lib.models.Song;
import maestro.my.music.R;
import maestro.my.music.Utils;
import maestro.my.music.data.Loders;
import maestro.my.music.data.MyMusicServiceListener;
import maestro.my.music.data.UniqAdapter;
import maestro.my.music.utils.SpaceItemDecoration;
import maestro.my.music.utils.TriangleBackground;
import maestro.svg.SVGInstance;

import java.util.ArrayList;

/**
 * Created by Artyom on 7/7/2015.
 */
public class SongsFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<ArrayList<Song>>, View.OnClickListener {

    public static final String TAG = SongsFragment.class.getSimpleName();

    private static final String PARAM_ITEM = SongsFragment.class.getSimpleName();

    private RecyclingImageView mImage;
    private UniqAdapter mAdapter;
    private RecyclerView mList;
    private View mHeaderView;
    private ImageView mActionButton;
    private LinearLayout mHeaderContent;
    private TextView mHeaderTitle;
    private TextView mHeaderAdditional;
    private TextView mHeaderAdditionalSmall;
    private ArrayList<Song> mSongs;
    private int mHeaderHeight;

    public static final SongsFragment makeInstance(Parcelable object) {
        SongsFragment fragment = new SongsFragment();
        Bundle args = new Bundle(1);
        args.putParcelable(PARAM_ITEM, object);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHeaderHeight = getResources().getDimensionPixelSize(R.dimen.big_image_height);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.songs_fragment_view, null);
        mImage = (RecyclingImageView) v.findViewById(R.id.image);
        mList = (RecyclerView) v.findViewById(R.id.list);
        mHeaderView = v.findViewById(R.id.header_view);
        mHeaderContent = (LinearLayout) v.findViewById(R.id.header_content);
        mHeaderTitle = (TextView) v.findViewById(R.id.title);
        mHeaderAdditional = (TextView) v.findViewById(R.id.additional);
        mHeaderAdditionalSmall = (TextView) v.findViewById(R.id.additional_small);
        mActionButton = (ImageView) v.findViewById(R.id.action_button);
        mList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mList.addItemDecoration(new SpaceItemDecoration(1, 0, getResources().getDimensionPixelSize(R.dimen.big_header_height), false));
        mList.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mHeaderView.setTranslationY(mHeaderView.getTranslationY() - dy);
                if (mImage != null && mHeaderView.getTranslationY() > -mHeaderHeight) {
                    mImage.setTranslationY(mHeaderView.getTranslationY() * -1 * 0.5f);
//                    float percent = (float) mHeaderView.getTop() * -1 / mHeaderHeight;
//                    if (percent > 0.9) {
//                        percent = 1.0f;
//                    }
//                    mToolbarBackground.setAlpha((int) (255 * percent));
                } else {
//                    mToolbarBackground.setAlpha(255);
                }
            }
        });

        mActionButton.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        mActionButton.setBackgroundDrawable(SVGInstance.getDrawable(R.raw.el_primary_action_button, getToolbarColor()));
        SVGInstance.applySVG(mActionButton, R.raw.ic_play, Color.WHITE, SVGInstance.DPI + 1.2f);

        mHeaderContent.setBackgroundDrawable(new TriangleBackground((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, getResources().getDisplayMetrics()), Color.WHITE));

        mActionButton.setOnClickListener(this);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mList.setAdapter(mAdapter = new UniqAdapter(getActivity(), UniqAdapter.TYPE.ROW, 1));
        mAdapter.setOnItemClickListener(new UniqAdapter.OnItemClickListener<Song>() {
            @Override
            public void onItemClick(UniqAdapter.Holder holder, Song item) {
                if (getService() != null) {
                    try {
                        getService().setSongsData(mSongs, holder.getAdapterPosition());
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    main.openPlayFragment();
                }
            }
        });
        mAdapter.setIsShowTime(true);
        applyItem();
        reattachServiceListener();
    }

    private void applyItem() {
        Parcelable mObject = getArguments().getParcelable(PARAM_ITEM);
        mImage.setLoadObject(MIM.by(Utils.MIM_IMAGE).to(mImage, Utils.getKey(mObject) + "_big")
                .object(mObject).displayer(new MIMAbstractDisplayer() {
                    @Override
                    public void display(View img) {
                        ObjectAnimator.ofFloat(img, View.TRANSLATION_Y, img.getHeight() / 7, 0).setDuration(225).start();
                    }
                }).cacheAnimationEnable(true));
        mAdapter.setIsShowIcon(!(mObject instanceof Album));
        if (getLoaderManager().getLoader(TAG.hashCode()) != null) {
            getLoaderManager().restartLoader(TAG.hashCode(), null, this);
        } else {
            getLoaderManager().initLoader(TAG.hashCode(), null, this);
        }
        mHeaderTitle.setText(Utils.getTitle(mObject));
        mHeaderAdditional.setText(Utils.getAdditional(getActivity(), mObject));
        mHeaderAdditionalSmall.setText(Utils.getAdditionalSmall(getActivity(), mObject));
    }

    @Override
    public Loader<ArrayList<Song>> onCreateLoader(int i, Bundle bundle) {
        return new Loders.SongsLoader(getActivity(), getArguments().getParcelable(PARAM_ITEM));
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<Song>> loader, ArrayList<Song> songs) {
        if (mAdapter != null) {
            mSongs = (ArrayList) songs;
            mAdapter.update((ArrayList) mSongs);
        }
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Song>> loader) {
    }

    @Override
    public boolean useSlideAnimation() {
        return true;
    }

    @Override
    public boolean isFullscreen() {
        return true;
    }

    @Override
    public String getKey() {
        return TAG;
    }

    @Override
    public MyMusicServiceListener getServiceListener() {
        return mAdapter != null ? mAdapter.getServiceListener() : null;
    }

    @Override
    public void onClick(View v) {
        Log.e(TAG, "onClick: " + v);
        switch (v.getId()) {
            case R.id.action_button:
                if (getService() != null) {
                    try {
                        getService().setSongsData(mSongs, 0);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    main.openPlayFragment();
                }
                break;
        }
    }
}