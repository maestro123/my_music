package maestro.my.music.ui;

import android.os.Bundle;
import android.support.v4.content.Loader;
import android.support.v7.widget.RecyclerView;
import com.msoft.android.mplayer.lib.models.Artist;
import com.msoft.android.mplayer.lib.models.Playlist;
import maestro.my.music.R;
import maestro.my.music.data.Loders;
import maestro.my.music.data.UniqAdapter;
import maestro.my.music.utils.TextItemDecoration;

import java.util.ArrayList;

/**
 * Created by Artyom on 7/6/2015.
 */
public class PlaylistFragment extends BaseContentFragment<Playlist> {

    public static final String TAG = PlaylistFragment.class.getSimpleName();

    private TextItemDecoration mDecoration;

    @Override
    public RecyclerView.ItemDecoration getDecoration() {
        return mDecoration = new TextItemDecoration(getActivity(), getSpanCount(),
                getResources().getDimensionPixelSize(R.dimen.row_item_spacing),
                getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material) * 2);
    }

    @Override
    public int getSpanCount() {
        return 1;
    }

    @Override
    public int getLoaderId() {
        return TAG.hashCode();
    }

    @Override
    public UniqAdapter.TYPE getAdapterType() {
        return UniqAdapter.TYPE.ROW;
    }

    @Override
    public Loader<ArrayList<Playlist>> onCreateLoader(int i, Bundle bundle) {
        return new Loders.PlaylistLoader(getActivity(), null);
    }

    @Override
    public String getKey() {
        return TAG;
    }
}
