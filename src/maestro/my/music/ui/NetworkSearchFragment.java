package maestro.my.music.ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import maestro.my.music.R;
import maestro.my.music.Utils;
import maestro.my.music.data.SearchItem;
import maestro.my.music.data.UniqAdapter;
import maestro.my.music.utils.SpaceItemDecoration;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Artyom on 8/12/2015.
 */
public class NetworkSearchFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Object> {

    public static final String TAG = HomeFragment.class.getSimpleName();

    private EditText mEditText;
    private RecyclerView mList;
    private UniqAdapter mAdapter;
    private View mEmptyView;
    private View mProgressBar;

    private String mQuery;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final int padding = getResources().getDimensionPixelSize(R.dimen.search_dialog_window_padding);
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        ColorDrawable mBackground = new ColorDrawable(Color.BLACK);
        mBackground.setAlpha(120);
        dialog.getWindow().setWindowAnimations(R.style.WindowAlphaAnimation);
        dialog.getWindow().setBackgroundDrawable(mBackground);
        dialog.getWindow().getDecorView().setPadding(padding, padding, padding, padding);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.search_view, null);
        mList = (RecyclerView) v.findViewById(R.id.list);
        mList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mList.addItemDecoration(new SpaceItemDecoration(getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material) * 2));
        mEditText = (EditText) v.findViewById(R.id.edit_text);
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mQuery = s.toString();
                search();
            }
        });
        mEmptyView = Utils.prepareEmptyView(v);
        mProgressBar = v.findViewById(R.id.progress_bar);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Utils.showInput(getActivity(), mEditText);
        mAdapter = new UniqAdapter(getActivity(), UniqAdapter.TYPE.ROW, 1);
        mAdapter.setIsShowTime(true);
        mAdapter.setIsShowIcon(false);
        mList.setAdapter(mAdapter);
        mList.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Utils.hideInput(getActivity(), mEditText);
            }
        });
    }

    @Override
    public Loader<Object> onCreateLoader(int i, Bundle bundle) {
        mEmptyView.animate().alpha(0f).start();
        mProgressBar.animate().alpha(1f).start();
        return new SearchLoader(getActivity(), mQuery);
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object o) {
        if (o instanceof List) {
            mAdapter.update((ArrayList) o);
        }
        updateEmptyView();
        mProgressBar.animate().alpha(0f).start();
    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {

    }

    @Override
    public String getKey() {
        return TAG;
    }

    private void search() {
        if (mQuery == null) {
            mAdapter.update(null);
            updateEmptyView();
            return;
        }
        if (getLoaderManager().getLoader(TAG.hashCode()) != null) {
            getLoaderManager().restartLoader(TAG.hashCode(), null, this);
        } else {
            getLoaderManager().initLoader(TAG.hashCode(), null, this);
        }
    }

    private void updateEmptyView() {
        if (mAdapter.getItemCount() == 0 && TextUtils.isEmpty(mQuery)) {
            mEmptyView.animate().alpha(0f).start();
            mList.animate().alpha(0f).start();
        } else if (mAdapter.getItemCount() == 0) {
            mEmptyView.animate().alpha(1f);
            mList.animate().alpha(0f);
        } else {
            mEmptyView.animate().alpha(0f).start();
            mList.animate().alpha(1f).start();
        }
    }

    public static class SearchLoader extends AsyncTaskLoader<Object> {

        private String mQuery;
        private String mUrl = "https://mp3skull.by/download/%s-mp3.html";

        public SearchLoader(Context context, String query) {
            super(context);
            mQuery = query;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public Object loadInBackground() {
            try {
                TagNode rootNode = new HtmlCleaner().clean(new URL(String.format(mUrl, mQuery.toLowerCase())).openStream());
                TagNode[] trItems = rootNode.getElementsByName("tr", true);
                ArrayList<SearchItem> mItems = new ArrayList<>();
                for (int i = 0; trItems != null && i < trItems.length; i++) {
                    TagNode[] nns = trItems[i].getElementsByAttValue("class", "tit", true, false);
                    if (nns.length == 0) continue;
                    SearchItem item = new SearchItem();
                    item.setTitle(nns[0].getText().toString());

                    nns = trItems[i].getElementsByAttValue("class", "dur", true, false);
                    if (nns.length == 0) continue;
                    item.setDuration(nns[0].getText().toString());

                    nns = trItems[i].getElementsByAttValue("class", "btn btn-success", true, false);
                    if (nns.length == 0) continue;
                    item.setUrl(nns[0].getAttributeByName("data-href"));

                    mItems.add(item);
                }
                return mItems;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    public boolean isFullscreenOnly() {
        return true;
    }

}