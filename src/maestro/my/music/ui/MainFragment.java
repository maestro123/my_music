package maestro.my.music.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import maestro.my.music.R;

/**
 * Created by Artyom on 7/19/2015.
 */
public class MainFragment extends IndicatorFragment {

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setAdapter(new MainPagerAdapter(getChildFragmentManager()));
    }

    private final class MainPagerAdapter extends FragmentStatePagerAdapter {

        public MainPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    return new HomeFragment();
                case 1:
                    return new AlbumFragment();
                case 2:
                    return new MusicFragment();
                case 3:
                    return new ArtistFragment();
                case 4:
                    return new GenreFragment();
                case 5:
                    return new PlaylistFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 6;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.home);
                case 1:
                    return getString(R.string.albums);
                case 2:
                    return getString(R.string.songs);
                case 3:
                    return getString(R.string.artists);
                case 4:
                    return getString(R.string.genres);
                case 5:
                    return getString(R.string.playlists);
            }
            return super.getPageTitle(position);
        }
    }

}
