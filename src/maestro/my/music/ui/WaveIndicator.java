package maestro.my.music.ui;

import android.content.Context;
import android.graphics.*;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import maestro.my.music.MyMusicApplication;
import maestro.my.music.data.MyMusicServiceVisualizerListener;

/**
 * Created by Artyom on 7/6/2015.
 */
public class WaveIndicator extends View {

    public static final String TAG = WaveIndicator.class.getSimpleName();

    private final RectF mRect = new RectF();

    private Paint mPaint = new Paint();
    private int mColor = Color.WHITE;
    private int mPickColor = mColor;
    private int defRound;
    private int defWidth;
    private int mSampleSize = 64;
    private int mMinus = 20;
    private byte[] mBytes;

    private MyMusicServiceVisualizerListener mListener = new MyMusicServiceVisualizerListener.Stub() {
        @Override
        public void onWaveFormDataCapture(byte[] waveform, int samplingRate) throws RemoteException {
            mBytes = waveform;
            postInvalidate();
        }
    };

    public WaveIndicator(Context context) {
        super(context);
        init();
    }

    public WaveIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WaveIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        defWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics());
        defRound = defWidth / 2;
        mPaint.setAntiAlias(true);
    }

    public void setColor(int color) {
        mColor = color;
    }

    public void setPickColor(int color) {
        mPickColor = color;
    }

    public void setDefWidth(int width) {
        defWidth = width;
        defRound = defWidth / 2;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        try {
            if (!isInEditMode())
                ((MyMusicApplication) getContext().getApplicationContext()).getService().attachVisualizerListener(this.toString(), mListener);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        try {
            ((MyMusicApplication) getContext().getApplicationContext()).getService().detachVisualizerListener(this.toString());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mColor == mPickColor) {
            mPaint.setColor(mColor);
        } else {
            mPaint.setShader(new LinearGradient(0, 0, 0, getHeight(), new int[]{mPickColor, mColor, mColor, mColor, mPickColor},
                    null, Shader.TileMode.MIRROR));
            mPaint.setAlpha((int) (255 * getAlpha()));
        }
        if (mBytes != null) {
            final int size = getWidth() / (defWidth * 2);
            final int step = mSampleSize / size;
            int left = getWidth() - (defWidth * 2 * size);
            byte value;
            float val;
            int height;
            for (int i = 0; i < size; i++) {
                value = mBytes[Math.min(mSampleSize - 1, i * step)];
                if (value < 0) {
                    value *= -1;
                }
                value -= mMinus;
                val = 1f - value / 128f;
                height = Math.max(defWidth, (int) (getHeight() * val));
//                mPaint.setColor(height > getHeight() / 3 ? mPickColor : mColor);
                mRect.set(left, getHeight() / 2 - height / 2, left + defWidth, getHeight() / 2 + height / 2);
                canvas.drawRoundRect(mRect, defRound, defRound, mPaint);
                left += defWidth * 2;
            }
        }
    }

}
