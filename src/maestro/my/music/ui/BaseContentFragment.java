package maestro.my.music.ui;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import maestro.my.music.R;
import maestro.my.music.data.Loders;
import maestro.my.music.data.UniqAdapter;
import maestro.my.music.utils.TextItemDecoration;

import java.util.ArrayList;

/**
 * Created by Artyom on 7/6/2015.
 */
public abstract class BaseContentFragment<T> extends BaseFragment implements LoaderManager.LoaderCallbacks<ArrayList<T>>,
        UniqAdapter.OnItemClickListener, UniqAdapter.OnItemLongClickListener {

    public static final String TAG = BaseContentFragment.class.getSimpleName();

    private RecyclerView mList;
    private UniqAdapter mAdapter;
    private RecyclerView.ItemDecoration mDecoration;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.base_content_view, null);
        mList = (RecyclerView) v.findViewById(R.id.list);
        mList.setOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (getParentFragment() instanceof IndicatorFragment) {
                    ((IndicatorFragment) getParentFragment()).dispatchScroll(dy);
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (getParentFragment() instanceof IndicatorFragment) {
                    ((IndicatorFragment) getParentFragment()).dispatchScrollStateChanged(newState);
                }
            }
        });
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mList.addItemDecoration(mDecoration = getDecoration());
        mList.setLayoutManager(new GridLayoutManager(getActivity(), getSpanCount()));
        mList.setAdapter(mAdapter = new UniqAdapter(getActivity(), getAdapterType(), getSpanCount()));
        mAdapter.setOnItemClickListener(this);
        mAdapter.setOnItemLongClickListener(this);
        load(false);
    }

    public final void load(boolean force) {
        if (force && getLoaderManager().getLoader(getLoaderId()) != null) {
            getLoaderManager().restartLoader(getLoaderId(), null, this);
        } else {
            getLoaderManager().initLoader(getLoaderId(), null, this);
        }
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<T>> loader, ArrayList<T> ts) {
        if (mAdapter != null) {
            if (mDecoration instanceof TextItemDecoration && loader instanceof Loders.IndexLoader) {
                ((TextItemDecoration) mDecoration).setDividers(((Loders.IndexLoader) loader).getDividers());
            }
            mAdapter.update((ArrayList) ts);
        }
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<T>> loader) {

    }

    @Override
    public void onItemClick(UniqAdapter.Holder holder, Object item) {

    }

    @Override
    public boolean onItemLongClick(UniqAdapter.Holder holder, Object item) {
        return false;
    }

    public abstract RecyclerView.ItemDecoration getDecoration();

    public abstract int getSpanCount();

    public abstract int getLoaderId();

    public abstract UniqAdapter.TYPE getAdapterType();

}