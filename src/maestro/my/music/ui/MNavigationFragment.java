package maestro.my.music.ui;

import android.app.Activity;
import android.view.View;
import maestro.my.music.Main;
import maestro.my.music.R;
import maestro.my.music.utils.ThemeHolder;
import maestro.support.v1.NavigationFragment;

import java.util.ArrayList;

/**
 * Created by Artyom on 7/19/2015.
 */
public class MNavigationFragment extends NavigationFragment {

    public static final String TAG = MNavigationFragment.class.getSimpleName();

    private Main main;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        main = (Main) activity;
    }

    @Override
    public View getHeader() {
        final View v = getLayoutInflater(null).inflate(R.layout.navigation_header, null);
        v.setBackgroundColor(ThemeHolder.getInstance().getToolbarColor());
        return v;
    }

    @Override
    public void onActionItemClick(ActionItem item) {
        main.openFragment(item.getId());
    }

    @Override
    public ArrayList<ActionItem> getItems() {
        ArrayList<ActionItem> mItems = new ArrayList<>();
        mItems.add(new ActionItem(Main.MAIN, getString(R.string.home)));
        mItems.add(new ActionItem(Main.ALBUMS, getString(R.string.albums)));
        mItems.add(new ActionItem(Main.MUSIC, getString(R.string.songs)));
        mItems.add(new ActionItem(Main.ARTISTS, getString(R.string.artists)));
        mItems.add(new ActionItem(Main.GENRES, getString(R.string.genres)));
        mItems.add(new ActionItem(Main.PLAYLISTS, getString(R.string.playlists)));
        mItems.add(new ActionItem(Main.RADIO_CATEGORIES, getString(R.string.radio_categories)));
        return mItems;
    }

    @Override
    public void applyIcon(NavigationFragment.NavigationAdapter.Holder holder, ActionItem item) {

    }
}
