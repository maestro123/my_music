package maestro.my.music.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import maestro.my.music.R;
import maestro.my.music.data.radio.DirbleAPI;
import maestro.my.music.data.radio.RadioCategoriesList;
import maestro.my.music.data.radio.RadioCategory;
import maestro.my.music.data.radio.RadioRequest;
import maestro.my.music.utils.SpaceItemDecoration;

/**
 * Created by Artyom on 7/19/2015.
 */
public class RadioGenresFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<RadioRequest> {

    public static final String TAG = RadioGenresFragment.class.getSimpleName();

    private RecyclerView mList;
    private RadioGenresAdapter mAdapter;
    private ProgressBar mProgressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.radio_genres_fragment_view, null);
        mList = (RecyclerView) v.findViewById(R.id.list);
        mList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mList.addItemDecoration(new SpaceItemDecoration(getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material) * 2));
        mProgressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
        mList.setAdapter(mAdapter = new RadioGenresAdapter(getActivity()));
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(TAG.hashCode(), null, this);
    }

    @Override
    public Loader<RadioRequest> onCreateLoader(int i, Bundle bundle) {
        return new CategoryLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<RadioRequest> loader, RadioRequest radioRequest) {
        if (radioRequest.getResult() != null) {
            mAdapter.update((RadioCategoriesList) radioRequest.getResult());
        }
        mProgressBar.animate().alpha(0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mProgressBar.setVisibility(View.GONE);
            }
        }).start();
    }

    @Override
    public void onLoaderReset(Loader<RadioRequest> loader) {

    }

    @Override
    public String getKey() {
        return TAG;
    }

    public final class RadioGenresAdapter extends RecyclerView.Adapter<RadioGenresAdapter.Holder> {

        private Context mContext;
        private LayoutInflater mInflater;
        private RadioCategoriesList mList;

        public RadioGenresAdapter(Context context) {
            mContext = context;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void update(RadioCategoriesList list) {
            mList = list;
            notifyDataSetChanged();
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup viewGroup, int i) {
            return new Holder(mInflater.inflate(R.layout.radio_categories_item_view, null));
        }

        @Override
        public void onBindViewHolder(Holder holder, int i) {
            RadioCategory category = mList.getStationAt(i);
            holder.Title.setText(category.getTitle());
            holder.Additional.setText(category.getDescription());
        }

        @Override
        public int getItemCount() {
            return mList != null ? mList.getSize() : 0;
        }

        public class Holder extends RecyclerView.ViewHolder {

            TextView Title;
            TextView Additional;

            public Holder(View itemView) {
                super(itemView);
                Title = (TextView) itemView.findViewById(R.id.title);
                Additional = (TextView) itemView.findViewById(R.id.additional);
            }
        }

    }

    public static final class CategoryLoader extends AsyncTaskLoader<RadioRequest> {

        public CategoryLoader(Context context) {
            super(context);
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public RadioRequest loadInBackground() {
            RadioRequest request = DirbleAPI.getAllCategories();
            request.process();
            return request;
        }

    }

}
