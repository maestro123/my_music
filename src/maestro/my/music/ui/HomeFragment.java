package maestro.my.music.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.msoft.android.mplayer.lib.MediaStoreHelper;
import maestro.my.music.R;
import maestro.my.music.data.HomeAdapter;
import maestro.my.music.utils.SpaceItemDecoration;
import maestro.xbox.music.XboxMusicAPI;

import java.util.ArrayList;

/**
 * Created by Artyom on 7/6/2015.
 */
public class HomeFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Object> {

    public static final String TAG = HomeFragment.class.getSimpleName();

    private static final String PARAM_TOP_TYPE = "topType";

    private RecyclerView mList;
    private HomeAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.home_fragment_view, null);
        mList = (RecyclerView) v.findViewById(R.id.list);
        mList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mList.addItemDecoration(new SpaceItemDecoration(getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material) * 2));
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mList.setAdapter(mAdapter = new HomeAdapter(getActivity(), getResources().getDisplayMetrics().widthPixels));
        mAdapter.addRecent(MediaStoreHelper.getAlbums(getActivity(), null));
        loadTops(TopsLoader.TOP_TYPE.FEATURED);
        loadTops(TopsLoader.TOP_TYPE.NEW);
        loadTops(TopsLoader.TOP_TYPE.ALBUMS);
        loadTops(TopsLoader.TOP_TYPE.TRACKS);
        loadTops(TopsLoader.TOP_TYPE.ARTISTS);
    }

    private void loadTops(TopsLoader.TOP_TYPE type) {
        Bundle args = new Bundle(1);
        args.putSerializable(PARAM_TOP_TYPE, type);
        getLoaderManager().initLoader(TAG.hashCode() + type.ordinal(), args, this);
    }

    @Override
    public Loader<Object> onCreateLoader(int i, Bundle bundle) {
        return new TopsLoader(getActivity(), (TopsLoader.TOP_TYPE) bundle.getSerializable(PARAM_TOP_TYPE));
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object o) {
        if (o instanceof ArrayList) {
            if (loader.getId() == TAG.hashCode() + TopsLoader.TOP_TYPE.FEATURED.ordinal()) {
                mAdapter.addTops("Featured", (ArrayList) o);
            } else if (loader.getId() == TAG.hashCode() + TopsLoader.TOP_TYPE.NEW.ordinal()) {
                mAdapter.addTops("New releases", (ArrayList) o);
            } else if (loader.getId() == TAG.hashCode() + TopsLoader.TOP_TYPE.ALBUMS.ordinal()) {
                mAdapter.addTops("Albums", (ArrayList) o);
            } else if (loader.getId() == TAG.hashCode() + TopsLoader.TOP_TYPE.TRACKS.ordinal()) {
                mAdapter.addTops("Tracks", (ArrayList) o);
            } else if (loader.getId() == TAG.hashCode() + TopsLoader.TOP_TYPE.ARTISTS.ordinal()) {
                mAdapter.addTops("Artists", (ArrayList) o);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {

    }

    @Override
    public String getKey() {
        return TAG;
    }

    public static class TopsLoader extends AsyncTaskLoader<Object> {

        public enum TOP_TYPE {
            FEATURED, NEW, ALBUMS, TRACKS, ARTISTS
        }

        private TOP_TYPE mType;

        public TopsLoader(Context context, TOP_TYPE type) {
            super(context);
            mType = type;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public Object loadInBackground() {
            Object result = XboxMusicAPI.getAppToken();
            if (result instanceof String) {
                String token = (String) result;
                String country = "US";
                switch (mType) {
                    case FEATURED:
                        return XboxMusicAPI.getFeatured(token, country, null);
                    case NEW:
                        return XboxMusicAPI.getNewReleases(token, country, null);
                    case ALBUMS:
                        return XboxMusicAPI.getAlbums(token, country, null);
                    case TRACKS:
                        return XboxMusicAPI.getTracks(token, country, null);
                    case ARTISTS:
                        return XboxMusicAPI.getArtists(token, country, null);
                }
            }
            return null;
        }
    }

}
