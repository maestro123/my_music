package maestro.my.music.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import maestro.my.music.R;

/**
 * Created by Artyom on 7/19/2015.
 */
public class RadioMainFragment extends IndicatorFragment {

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setAdapter(new RadioHomePagerAdapter(getChildFragmentManager()));
    }

    private final class RadioHomePagerAdapter extends FragmentStatePagerAdapter {

        public RadioHomePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    return new RadioGenresFragment();
                case 1:
                    return new RadioStationsFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.categories);
                case 1:
                    return getString(R.string.stations);
            }
            return super.getPageTitle(position);
        }

        @Override
        public float getPageWidth(int position) {
            return position == 0 ? 0.7f : 1f;
        }
    }

}
