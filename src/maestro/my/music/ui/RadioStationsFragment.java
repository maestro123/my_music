package maestro.my.music.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.*;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.dream.android.mim.ImageLoadObject;
import com.dream.android.mim.MIM;
import maestro.my.music.R;
import maestro.my.music.Utils;
import maestro.my.music.data.radio.*;
import maestro.my.music.utils.SpaceItemDecoration;
import org.apmem.tools.layouts.FlowLayout;

import java.util.Locale;

/**
 * Created by Artyom on 7/20/2015.
 */
public class RadioStationsFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<RadioRequest> {

    public static final String TAG = RadioStationsFragment.class.getSimpleName();

    private RecyclerView mList;
    private ProgressBar mProgress;
    private RadioStationsAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.radio_stations_fragment_view, null);
        mList = (RecyclerView) v.findViewById(R.id.list);
        mList.setLayoutManager(new StaggeredGridLayoutManager(getSpanCount(), StaggeredGridLayoutManager.VERTICAL));
        mList.addItemDecoration(new SpaceItemDecoration(getSpanCount(),
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics()),
                getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material) * 2
                        + (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics()), false));
        mProgress = (ProgressBar) v.findViewById(R.id.progress_bar);
        return v;
    }

    private int getSpanCount() {
        return getResources().getInteger(R.integer.grid_columns);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mList.setAdapter(mAdapter = new RadioStationsAdapter(getActivity(), getSpanCount()));
        getLoaderManager().initLoader(TAG.hashCode(), null, this);
    }

    @Override
    public Loader<RadioRequest> onCreateLoader(int i, Bundle bundle) {
        return new RadioStationsLoader(getActivity(), -1);
    }

    @Override
    public void onLoadFinished(Loader<RadioRequest> loader, RadioRequest radioRequest) {
        if (radioRequest.getResult() != null) {
            mAdapter.update((RadioStationList) radioRequest.getResult());
        }
        mProgress.animate().alpha(0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mProgress.setVisibility(View.GONE);
            }
        }).start();
    }

    @Override
    public void onLoaderReset(Loader<RadioRequest> loader) {

    }

    @Override
    public String getKey() {
        return TAG;
    }

    public final class RadioStationsAdapter extends RecyclerView.Adapter<RadioStationsAdapter.Holder>
            implements View.OnLongClickListener, View.OnClickListener {

        private Context mContext;
        private LayoutInflater mInflater;
        private RadioStationList mList;
        private int textPadding;
        private int textBackgroundRadius;
        private int width;

        public RadioStationsAdapter(Context context, int spanCount) {
            mContext = context;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final Resources resources = context.getResources();
            final boolean isTablet = resources.getBoolean(R.bool.is7inch) || resources.getBoolean(R.bool.is10inch);
            final boolean isLandscape = resources.getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
            width = (int) ((resources.getDisplayMetrics().widthPixels * (isTablet && isLandscape ? 0.65f : 1f)) / spanCount);
            textBackgroundRadius = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
            textPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics());
        }

        public void update(RadioStationList list) {
            mList = list;
            notifyDataSetChanged();
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup viewGroup, int i) {
            final View v = mInflater.inflate(R.layout.radio_station_item_view, null);
            v.setOnClickListener(this);
            v.setOnLongClickListener(this);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(Holder holder, int i) {
            RadioStation station = mList.getStationAt(i);
            holder.Title.setText(station.getName());

            StringBuilder builder = new StringBuilder();
            RadioStream stream = station.getHighestStream();
            if (stream != null) {
                builder.append(stream.getBitrate()).append(" kb");
            }

            for (Locale locale : Locale.getAvailableLocales()) {
                if (locale.getCountry().equalsIgnoreCase(station.getCountry())) {
                    boolean start = builder.length() > 0;
                    if (start)
                        builder.append(" (");
                    builder.append(locale.getDisplayCountry());
                    if (start)
                        builder.append(")");
                    break;
                }
            }
            holder.Additional.setText(builder.toString());

            if (station.getImageWidth() > 0) {
                holder.Image.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.Image.getLayoutParams();
                params.height = (int) (station.getImageHeight() * ((float) width / station.getImageWidth()));
                MIM.by(Utils.MIM_NETWORK).to(holder.Image, station.getImageUrl()).async();
            } else {
                holder.Image.setVisibility(View.GONE);
                ImageLoadObject.cancel(holder.Image);
            }

            RadioCategory[] categories = station.getCategories();
            holder.CategoriesLayout.removeAllViews();
            if (categories != null && categories.length > 0) {
                for (int j = 0; j < categories.length; j++) {
                    View view = getTextView(categories[j].getTitle());
                    holder.CategoriesLayout.addView(view);
                    ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).setMargins(textPadding / 2, textPadding / 2, textPadding / 2, textPadding / 2);
                }
            }

            holder.itemView.setTag(station);

        }

        private TextView getTextView(String title) {
            TextView text = new TextView(mContext);
            text.setText(title);
            text.setTextColor(Color.WHITE);
            text.setPadding(textPadding, textPadding / 3, textPadding, textPadding / 3);
            text.setBackgroundDrawable(new RDrawable(Utils.getTextColor(title), textBackgroundRadius));
            return text;
        }

        @Override
        public void onClick(View v) {
            RadioStation station = (RadioStation) v.getTag();
            if (getService() != null && station != null) {
                try {
                    getService().setRadioData(station, 0);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public boolean onLongClick(View v) {
            return false;
        }

        private class RDrawable extends Drawable {

            private Paint mPaint;
            private int mColor;
            private int mRadius;

            public RDrawable(int color, int radius) {
                mColor = color;
                mRadius = radius;
                mPaint = new Paint();
                mPaint.setColor(color);
                mPaint.setAntiAlias(true);
            }

            @Override
            public void draw(Canvas canvas) {
                Rect rect = getBounds();
                canvas.drawRoundRect(new RectF(rect), rect.height() / 2, rect.height() / 2, mPaint);
            }

            @Override
            public void setAlpha(int alpha) {

            }

            @Override
            public void setColorFilter(ColorFilter cf) {

            }

            @Override
            public int getOpacity() {
                return 0;
            }
        }

        @Override
        public int getItemCount() {
            return mList != null ? mList.getSize() : 0;
        }

        public class Holder extends RecyclerView.ViewHolder {

            private ImageView Image;
            private TextView Title;
            private TextView Additional;
            private FlowLayout CategoriesLayout;

            public Holder(View itemView) {
                super(itemView);
                Image = (ImageView) itemView.findViewById(R.id.image);
                Title = (TextView) itemView.findViewById(R.id.title);
                Additional = (TextView) itemView.findViewById(R.id.additional);
                CategoriesLayout = (FlowLayout) itemView.findViewById(R.id.flow_layout);
            }

        }

    }

    public static final class RadioStationsLoader extends AsyncTaskLoader<RadioRequest> {

        private int mCategoryId = -1;

        public RadioStationsLoader(Context context, int id) {
            super(context);
            mCategoryId = id;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public RadioRequest loadInBackground() {
            long startTime = System.currentTimeMillis();
            RadioRequest request;
            if (mCategoryId != -1) {
                request = DirbleAPI.getCategoryStations(mCategoryId);
            } else {
                request = DirbleAPI.getStations();
            }
            request.process();
            Log.e(TAG, "end time: " + (System.currentTimeMillis() - startTime));
            return request;
        }

    }

}
