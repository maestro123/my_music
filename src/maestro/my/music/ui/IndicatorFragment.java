package maestro.my.music.ui;

import android.animation.ValueAnimator;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import maestro.my.music.R;
import maestro.my.music.utils.PagerIndicator;
import maestro.svg.SVGInstance;

/**
 * Created by Artyom on 7/19/2015.
 */
public class IndicatorFragment extends BaseFragment {

    public static final String TAG = IndicatorFragment.class.getSimpleName();

    private static final float TRANSLATION_FACTOR = 0.7f;

    private ViewPager mPager;
    private PagerIndicator mIndicator;
    private FragmentStatePagerAdapter mAdapter;
    private Toolbar mToolbar;
    private View mShadow;

    private int mToolbarHeight;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mToolbarHeight = getResources().getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.indicator_fragment_view, null);
        mToolbar = (Toolbar) v.findViewById(R.id.toolbar);
        mPager = (ViewPager) v.findViewById(R.id.pager);
        mIndicator = (PagerIndicator) v.findViewById(R.id.indicator);
        mShadow = v.findViewById(R.id.shadow);
        mPager.setOffscreenPageLimit(4);
        mToolbar.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        mToolbar.setNavigationIcon(SVGInstance.getDrawable(R.raw.ic_menu, Color.WHITE));
        mIndicator.setOnPageChangeListener(mPageChangeListener);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        main.setSupportActionBar(mToolbar, mIndicator);
    }

    public void setAdapter(FragmentStatePagerAdapter adapter) {
        mAdapter = adapter;
        mPager.setAdapter(mAdapter);
        mIndicator.setViewPager(mPager);
    }

    @Override
    public String getKey() {
        return TAG;
    }

    private ValueAnimator mScrollAnimator;
    private int scrollY;
    private int totalY;
    private int currentTranslation;

    public void dispatchScroll(int dy) {
        if (scrollY > 0 && dy < 0 || scrollY < 0 && dy > 0) {
            scrollY = 0;
        }
        scrollY += dy;
        int translation = currentTranslation + dy;
        if (translation < 0) {
            setViewTranslations(0);
        } else if (translation < mToolbarHeight) {
            setViewTranslations(translation);
        } else if (translation > mToolbarHeight) {
            setViewTranslations(mToolbarHeight);
        }
        totalY += dy;
    }

    public void dispatchScrollStateChanged(int scrollState) {
        if (scrollState == RecyclerView.SCROLL_STATE_IDLE) {
            if (scrollY > 0) {
                if (scrollY > 0 && totalY >= mToolbarHeight) {
                    animateTranslation(mToolbarHeight);
                } else {
                    animateTranslation(0);
                }
            } else {
                animateTranslation(0);
            }
            scrollY = 0;
        }
    }

    private void animateTranslation(final int y) {
        mScrollAnimator = new ValueAnimator();
        mScrollAnimator.setIntValues(currentTranslation, y);
        mScrollAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                setViewTranslations((Integer) animation.getAnimatedValue());
            }
        });
        mScrollAnimator.start();
    }

    private final void setViewTranslations(int y) {
        currentTranslation = y;
        mToolbar.setTranslationY(-y);
        mIndicator.setTranslationY(-y);
        mShadow.setTranslationY(-y);
    }

    private ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.SimpleOnPageChangeListener(){
        @Override
        public void onPageScrollStateChanged(int state) {
            super.onPageScrollStateChanged(state);
            if (state != ViewPager.SCROLL_STATE_IDLE){
                animateTranslation(0);
            }
        }
    };

}