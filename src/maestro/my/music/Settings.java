package maestro.my.music;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Artyom on 8/9/2015.
 */
public class Settings {

    private static volatile Settings instance;

    public static synchronized Settings getInstance(){
        return instance != null ? instance : (instance = new Settings());
    }

    private Context mContext;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;

    public void initialize(Context context){
        mContext = context;
        mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mEditor = mPreferences.edit();
    }

    private static final String PREF_IS_FIRST_START = "pref_first_start";

    public void setIsFirstStart(boolean isFirstStart){
        mEditor.putBoolean(PREF_IS_FIRST_START, isFirstStart).apply();
    }

    public boolean isFirstStart(){
        return mPreferences.getBoolean(PREF_IS_FIRST_START, true);
    }

}
