package maestro.my.music.utils;

import android.util.Log;

/**
 * Created by Artyom on 7/23/2015.
 */
public class E {

    private static boolean isDebug = true;

    public static void e(String tag, String message) {
        if (isDebug) {
            Log.e(tag, message);
        }
    }

}
