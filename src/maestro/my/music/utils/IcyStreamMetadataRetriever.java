package maestro.my.music.utils;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Artyom on 9/28/2015.
 */
public class IcyStreamMetadataRetriever {

    public static final String TAG = IcyStreamMetadataRetriever.class.getSimpleName();

    private static final String PARAM_TITLE = "StreamTitle";
    private static final String PARAM_META_URL = "StreamUrl";
    private static final String PARAM_GENRE = "icy-genre";
    private static final String PARAM_BITRATE = "icy-br";
    private static final String PARAM_NAME = "icy-name";
    private static final int DEF_RETRY_COUNT = 10;
    private static final long DEF_RELOAD_DELAY = 1500;

    private Map<String, String> metadata;
    private ArrayList<OnMetadataUpdateListener> mListeners = new ArrayList<>();

    public interface OnMetadataUpdateListener {
        void onMetadataUpdate(Map<String, String> metadata);
    }

    public IcyStreamMetadataRetriever() {
    }

    public String getStreamTitle() {
        return metadata == null || !metadata.containsKey(PARAM_TITLE) ? "" : metadata.get(PARAM_TITLE).trim();
    }

    public String getArtist() {
        if (metadata == null || !metadata.containsKey("StreamTitle")) {
            return "";
        }
        final String streamTitle = metadata.get("StreamTitle");
        final int index = streamTitle.indexOf("-");
        if (index > 0) {
            String title = streamTitle.substring(0, streamTitle.indexOf("-"));
            return title.trim();
        } else {
            return "";
        }
    }

    public String getTitle() {
        if (metadata == null || !metadata.containsKey("StreamTitle")) {
            return "";
        }
        String streamTitle = metadata.get("StreamTitle");
        String artist = streamTitle.substring(streamTitle.indexOf("-") + 1);
        return artist.trim();
    }

    public void addOnMetadataUpdateListener(OnMetadataUpdateListener listener) {
        synchronized (mListeners) {
            mListeners.add(listener);
        }
    }

    public void removeOnMetadataUpdateListener(OnMetadataUpdateListener listener) {
        synchronized (mListeners) {
            mListeners.remove(listener);
        }
    }

    public void retrieveMetadata(String url) {
        try {
            URLConnection con = new URL(url).openConnection();
            con.setRequestProperty("Icy-MetaData", "1");
            con.setRequestProperty("Connection", "close");
            con.connect();

            int metaDataOffset = 0;
            Map<String, List<String>> headers = con.getHeaderFields();
            InputStream stream = con.getInputStream();

            if (headers.containsKey("icy-metaint")) {
                metaDataOffset = Integer.parseInt(headers.get("icy-metaint").get(0));
            }

            if (metaDataOffset == 0) {
                throw new Exception();
            }

            int b;
            int count = 0;
            int metaDataLength = 4080; // 4080 is the max length
            boolean inData = false;
            StringBuilder metaData = new StringBuilder();
            while ((b = stream.read()) != -1) {
                count++;

                // Length of the metadata
                if (count == metaDataOffset + 1) {
                    metaDataLength = b * 16;
                }

                if (count > metaDataOffset + 1 && count < (metaDataOffset + metaDataLength)) {
                    inData = true;
                } else {
                    inData = false;
                }
                if (inData) {
                    if (b != 0) {
                        metaData.append((char) b);
                    }
                }
                if (count > (metaDataOffset + metaDataLength)) {
                    break;
                }

            }
            metadata = IcyStreamMetadataRetriever.parseMetadata(metaData.toString());
            stream.close();
        } catch (Exception e) {
            e.printStackTrace();
            metadata = null;
        }
    }

    private static Map<String, String> parseMetadata(String metaString) {
        Map<String, String> metadata = new HashMap<String, String>();
        String[] metaParts = metaString.split(";");
        Pattern p = Pattern.compile("^([a-zA-Z]+)=\\'(.*)\\'$"); //match pattern <characters>='<any>'
        Matcher m;
        for (int i = 0; i < metaParts.length; i++) {
            m = p.matcher(metaParts[i]);
            if (m.find()) {
                String key = ((String) m.group(1)).trim();
                String value = ((String) m.group(2)).trim();
                metadata.put(key, value);
            }
        }
        return metadata;
    }

    private RetrieveThread mRetrieveThread;
    private final Handler uiHandler = new Handler(Looper.getMainLooper()) {

        private Map<String, String> mPreviousMap;

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (mPreviousMap == null || !mPreviousMap.equals(metadata)) {
                mPreviousMap = metadata;
                synchronized (mListeners) {
                    for (OnMetadataUpdateListener listener : mListeners) {
                        listener.onMetadataUpdate(metadata);
                    }
                }
            }
        }
    };

    public void startRetrieve(final String url) {
        if (mRetrieveThread == null) {
            mRetrieveThread = new RetrieveThread(url);
            mRetrieveThread.start();
        }
    }

    public void restart(String url) {
        stop();
        startRetrieve(url);
    }

    public void stop() {
        if (mRetrieveThread != null) {
            mRetrieveThread.forceStop();
            mRetrieveThread = null;
        }
    }

    private final class RetrieveThread extends Thread {

        private String url;
        private boolean isStopped;
        private int mRetryCount = DEF_RETRY_COUNT;

        public RetrieveThread(String url) {
            this.url = url;
        }

        @Override
        public void run() {
            super.run();
            while (!isInterrupted() && !isStopped) {
                retrieveMetadata(url);
                long sleepTime = DEF_RELOAD_DELAY;
                if (metadata == null && mRetryCount > 0) {
                    sleepTime = 350;
                    //TODO: to do something?
                    mRetryCount--;
                } else {
                    mRetryCount = DEF_RETRY_COUNT;
                    uiHandler.sendEmptyMessage(0);
                }
                try {
                    sleep(sleepTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public void forceStop() {
            isStopped = true;
            interrupt();
        }

    }

}
