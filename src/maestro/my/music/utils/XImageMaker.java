package maestro.my.music.utils;

import android.content.Context;
import android.graphics.Bitmap;
import com.dream.android.mim.ImageLoadObject;
import com.dream.android.mim.MIMInternetMaker;
import maestro.xbox.music.models.BaseXObject;

/**
 * Created by Artyom on 7/8/2015.
 */
public class XImageMaker extends MIMInternetMaker {

    public XImageMaker() {
    }

    @Override
    public Bitmap getBitmap(ImageLoadObject loadObject, Context ctx) {
        if (loadObject.getObject() instanceof BaseXObject) {
            BaseXObject xObject = (BaseXObject) loadObject.getObject();
            loadObject.path(xObject.getImageUrl(loadObject.getWidth(), loadObject.getHeight()));
        }
        return super.getBitmap(loadObject, ctx);
    }
}
