package maestro.my.music.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;

import java.util.HashMap;

/**
 * Created by Artyom on 7/6/2015.
 */
public class TextItemDecoration extends RecyclerView.ItemDecoration {

    public static final String TAG = TextItemDecoration.class.getSimpleName();

    private HashMap<Integer, String> mDividers;
    private Paint mTextPaint = new Paint();
    private int textSidePadding;
    private int textTopPadding;
    private int spanCount;
    private int spacing;
    private int topSpacing;

    public TextItemDecoration(Context context, int spanCount, int spacing, int topSpacing) {
        this.spanCount = spanCount;
        this.spacing = spacing;
        this.topSpacing = topSpacing;
        textSidePadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, context.getResources().getDisplayMetrics());
        mTextPaint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, context.getResources().getDisplayMetrics()));
        mTextPaint.setColor(Color.BLACK);
        mTextPaint.setAntiAlias(true);

        Rect bounds = new Rect();
        mTextPaint.getTextBounds("A", 0, 1, bounds);
        textTopPadding -= bounds.height() / 2;
    }

    public void setDividers(HashMap<Integer, String> dividers) {
        mDividers = dividers;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        if (position < spanCount) {
            outRect.top = topSpacing;
        }
        if (mDividers != null && mDividers.size() > 0) {
            if (mDividers.containsKey(position)) {
                int column = position % spanCount;

                outRect.left = column * spacing / spanCount;
                outRect.right = spacing - (column + 1) * spacing / spanCount;
                outRect.top += spacing;
            }
        }

    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDrawOver(c, parent, state);
        if (mDividers != null && mDividers.size() > 0) {
            final int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View v = parent.getChildAt(i);
                int position = parent.getChildAdapterPosition(v);

                if (mDividers.containsKey(position)) {
                    c.drawText(mDividers.get(position), v.getX() + textSidePadding, v.getY() - spacing / 2 - textTopPadding, mTextPaint);
                }
            }
        }
    }
}
