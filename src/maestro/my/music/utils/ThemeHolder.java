package maestro.my.music.utils;

import android.content.Context;
import android.graphics.Color;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

/**
 * Created by Artyom on 7/6/2015.
 */
public class ThemeHolder {

    public static final String TAG = ThemeHolder.class.getSimpleName();

    private static volatile ThemeHolder instance;

    public static final synchronized ThemeHolder getInstance() {
        return instance != null ? instance : (instance = new ThemeHolder());
    }

    private static final String TAG_TOOLBAR_COLOR = "toolbarColor";
    private static final String ATTR_NAMESPACE = "mmt";
    private static final String ATTR_COLOR = "color";

    private int mToolbarColor = Color.parseColor("#f44336");

    public int getToolbarColor() {
        return mToolbarColor;
    }

    public void loadTheme(Context context, String path) {
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();
            parser.setInput(context.getAssets().open(path), "UTF-8");
            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    if (parser.getName().equals(TAG_TOOLBAR_COLOR)) {
                        mToolbarColor = getColor(parser);
                    }
                }
                eventType = parser.next();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getColor(XmlPullParser parser) {
        return Color.parseColor(parser.getAttributeValue(ATTR_NAMESPACE, ATTR_COLOR));
    }

}
