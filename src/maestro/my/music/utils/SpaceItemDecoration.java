package maestro.my.music.utils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Artyom on 6/29/2015.
 */
public class SpaceItemDecoration extends RecyclerView.ItemDecoration {

    private int spanCount = 1;
    private int spacing;
    private int topSpacing;
    private boolean includeEdge;

    public SpaceItemDecoration(int spanCount, int spacing, int topSpacing, boolean includeEdge) {
        this.spanCount = spanCount;
        this.spacing = spacing;
        this.includeEdge = includeEdge;
        this.topSpacing = topSpacing;
    }

    public SpaceItemDecoration(int topSpacing) {
        this.topSpacing = topSpacing;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        int column = position % spanCount;
        if (includeEdge) {
            outRect.left = spacing - column * spacing / spanCount;
            outRect.right = (column + 1) * spacing / spanCount;

            if (position < spanCount) {
                outRect.top = topSpacing;
            }
            outRect.bottom = spacing;
        } else {
            outRect.left = column * spacing / spanCount;
            outRect.right = spacing - (column + 1) * spacing / spanCount;
            if (position >= spanCount) {
                outRect.top = spacing;
            } else {
                outRect.top = topSpacing;
            }
        }
    }
}