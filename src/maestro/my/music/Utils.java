package maestro.my.music;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import com.msoft.android.mplayer.lib.models.Album;
import com.msoft.android.mplayer.lib.models.Artist;
import com.msoft.android.mplayer.lib.models.Genre;
import com.msoft.android.mplayer.lib.models.Song;
import maestro.my.music.data.SearchItem;
import maestro.my.music.data.radio.RadioStation;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by Artyom on 7/6/2015.
 */
public class Utils {

    public static final String MIM_IMAGE = "mim_image";
    public static final String MIM_X_NETWORK = "mim_x_network";
    public static final String MIM_NETWORK = "mim_network";

    final static String alphabet = "abcdefghijklmnopqrstuvwxyz";

    public static String getKey(Object object) {
        if (object instanceof Album) {
            return ((Album) object).Title;
        } else if (object instanceof Song) {
            return ((Song) object).Title;
        } else if (object instanceof Genre) {
            return ((Genre) object).Title;
        } else if (object instanceof Artist) {
            return ((Artist) object).Title;
        } else if (object instanceof RadioStation) {
            return ((RadioStation) object).getWebSite();
        }
        return null;
    }

    public static String getTitle(Object object) {
        if (object instanceof Album) {
            return ((Album) object).Title;
        } else if (object instanceof Song) {
            String title = ((Song) object).Title;
            if (!TextUtils.isEmpty(title)) {
                if (title.matches("\\d.+")) {
                    title = title.substring(title.indexOf(".") + 1);
                }
            }
            return title;
        } else if (object instanceof Genre) {
            return ((Genre) object).Title;
        } else if (object instanceof Artist) {
            return ((Artist) object).Title;
        } else if (object instanceof RadioStation) {
            return ((RadioStation) object).getName();
        } else if (object instanceof SearchItem) {
            return ((SearchItem) object).getTitle();
        }
        return null;
    }

    public static String getAdditional(Context context, Object object) {
        if (object instanceof Album) {
            return ((Album) object).author;
        } else if (object instanceof Song) {
            return ((Song) object).Author;
        } else if (object instanceof Genre) {
            return getCountString(context, ((Genre) object).getCountInt());
        } else if (object instanceof Artist) {
            return getCountString(context, ((Artist) object).Count);
        } else if (object instanceof RadioStation) {
            return ((RadioStation) object).getDescription();
        }
        return null;
    }

    public static String getAdditionalSmall(Context context, Object object) {
        if (object instanceof Album) {
            return ((Album) object).FirstYear;
        } else if (object instanceof Song) {
        } else if (object instanceof Genre) {
        } else if (object instanceof Artist) {
        }
        return null;
    }

    private static String getCountString(Context context, int count) {
        StringBuilder builder = new StringBuilder();
        builder.append(count).append(" ");
        if (count == 1) {
            builder.append(context.getString(R.string.c_song));
        } else {
            builder.append(context.getString(R.string.c_songs));
        }
        return builder.toString();
    }

    public static long getDuration(Object object) {
        if (object instanceof Song) {
            return ((Song) object).Duration;
        } else if (object instanceof SearchItem) {
            SimpleDateFormat formatter = new SimpleDateFormat("mm:ss");
            try {
                return formatter.parse(((SearchItem) object).getDuration()).getTime();
            } catch (ParseException e) {
                e.printStackTrace();

            }
        }
        return 0;
    }

    public static final int getTextColor(String string) {
        int left = getLeft(string, 3);
        int first = getFirst(string);
        int color = div(string.hashCode(), 255, true);

        int r = -1, g = -1, b = -1;

        if (left == 0) {
            r = 0;
        } else if (left == 1) {
            g = 0;
        } else if (left == 2) {
            b = 0;
        }

        if (left == first) {
            if (left == 0) {
                first = 2;
            } else {
                first = 0;
            }
        }

        if (first == 0) {
            r = 255;
        } else if (first == 1) {
            g = 255;
        } else if (first == 2) {
            b = 255;
        }

        if (r == -1) {
            r = color;
        } else if (g == -1) {
            g = color;
        } else if (b == -1) {
            b = color;
        }

        return Color.rgb(r, g, b);
    }

    static final int div(int input, int divver, boolean minus) {
        if (input < 0) {
            input *= -1;
        }
        int result = input / divver;
        if (minus && result == 1) {
            result = input - divver;
        }
        if (result > divver) {
            return div(result, divver, minus);
        } else {
            return result;
        }
    }

    static final int getLeft(String string, int divver) {
        final int fullDiv = string.length() / divver;
        return string.length() - divver * fullDiv;
    }

    static final int getFirst(String string) {
        final int size = string.length();
        final int row = size / 3 - 1;
        String str = string.substring(0, Math.min(3, Math.max(3, row)));
        int totalIndex = 0;
        for (int i = 0; i < str.length(); i++) {
            totalIndex += alphabet.indexOf(str.charAt(i)) + 1;
        }
        int result = div(totalIndex, 3, true);
        return Math.min(2, result);
    }

    public static View prepareEmptyView(View parent) {
        return prepareEmptyView(parent, parent.getResources().getString(R.string.nothing_here));
    }

    public static View prepareEmptyView(View parent, String title) {
        TextView txtTitle = (TextView) parent.findViewById(R.id.title);
        txtTitle.setText(title);
        return parent.findViewById(R.id.empty_view_parent);
    }

    public static final void hideInput(Activity activity, EditText mEditText) {
        mEditText.clearFocus();
        InputMethodManager manager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.hideSoftInputFromInputMethod(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static final void showInput(Activity activity, EditText editText) {
        editText.requestFocus();
        InputMethodManager manager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.showSoftInput(editText, 0);
    }

}
