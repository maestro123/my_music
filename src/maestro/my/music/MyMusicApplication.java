package maestro.my.music;

import android.app.Application;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import com.dream.android.mim.MIM;
import com.dream.android.mim.MIMDiskCache;
import com.dream.android.mim.MIMInternetMaker;
import com.dream.android.mim.MIMManager;
import com.msoft.android.service.MusicPlayerEngine;
import maestro.lastfm.lib.data.LastFmDBHelper;
import maestro.my.music.data.Database;
import maestro.my.music.data.IMyMusicService;
import maestro.my.music.data.MusicImageMaker;
import maestro.my.music.data.MyMusicService;
import maestro.my.music.utils.ThemeHolder;
import maestro.my.music.utils.XImageMaker;
import maestro.svg.SVGInstance;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Artyom on 7/6/2015.
 */
public class MyMusicApplication extends Application {

    public static final String TAG = MyMusicApplication.class.getSimpleName();

    private IMyMusicService mService;
    private ArrayList<OnServiceConnectedListener> mListeners = new ArrayList<>();

    public interface OnServiceConnectedListener {
        void onServiceConnected(IMyMusicService service);

        void onServiceDisconnected(IMyMusicService service);
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = IMyMusicService.Stub.asInterface(service);
            notifyServiceConnected();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        new Database(this);

        SVGInstance.initialize(this);
        LastFmDBHelper.getInstance().init(this);
        MusicPlayerEngine.getInstance().initialize(this);
        ThemeHolder.getInstance().loadTheme(this, "default.xml");
        Settings.getInstance().initialize(this);

        bindService(new Intent(this, MyMusicService.class), mConnection, BIND_AUTO_CREATE);

        MIMDiskCache mimDiskCache = MIMDiskCache.getInstance().init(getCacheDir(), 200 * 1024 * 1024);
        MIMManager.getInstance().addMIM(Utils.MIM_IMAGE, new MIM(this).maker(new MusicImageMaker()));//.setDiskCache(MIMDiskCache.getInstance().init(getCacheDir().getPath())));
        MIMManager.getInstance().addMIM(Utils.MIM_X_NETWORK, new MIM(this).maker(new XImageMaker()).setDiskCache(mimDiskCache));
        MIMManager.getInstance().addMIM(Utils.MIM_NETWORK, new MIM(this).maker(new MIMInternetMaker()).setDiskCache(mimDiskCache));

        new Thread() {
            @Override
            public void run() {
                super.run();
                HttpURLConnection urlConnection = null;
                try {
                    urlConnection = (HttpURLConnection) new URL("http://localhost:8080/").openConnection();
                    Log.e(TAG, "from server: " + read(urlConnection.getInputStream()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e(TAG, "just line");
            }
        }.start();

    }

    private static String read(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder out = new StringBuilder();
        String newLine = System.getProperty("line.separator");
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                out.append(line);
                out.append(newLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return out.toString();
    }


    public IMyMusicService getService() {
        return mService;
    }

    public void attachOnServiceConnectedListener(OnServiceConnectedListener listener) {
        if (mService != null) {
            listener.onServiceConnected(mService);
        } else {
            mListeners.add(listener);
        }
    }

    public void detachOnServiceConnectedListener(OnServiceConnectedListener listener) {
        mListeners.remove(listener);
        if (listener != null && mService != null) {
            listener.onServiceDisconnected(mService);
        }
    }

    public void notifyServiceConnected() {
        synchronized (mListeners) {
            for (OnServiceConnectedListener listener : mListeners) {
                listener.onServiceConnected(mService);
            }
            mListeners.clear();
        }
    }

}
