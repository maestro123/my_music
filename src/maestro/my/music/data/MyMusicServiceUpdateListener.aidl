package maestro.my.music.data;

interface MyMusicServiceUpdateListener {

    void onPlayTimeUpdate(int current, int total);

    void onRadioInfoUpdate(in Map info);

}