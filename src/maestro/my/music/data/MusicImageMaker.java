package maestro.my.music.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import com.dream.android.mim.ImageLoadObject;
import com.dream.android.mim.MIMDefaultMaker;
import com.msoft.android.mplayer.lib.MediaStoreHelper;
import com.msoft.android.mplayer.lib.models.Album;
import com.msoft.android.mplayer.lib.models.Artist;
import com.msoft.android.mplayer.lib.models.Genre;
import com.msoft.android.mplayer.lib.models.Song;
import maestro.lastfm.lib.LastFmApi;
import maestro.lastfm.lib.LastFmArtist;
import maestro.lastfm.lib.LastFmTrack;
import maestro.my.music.R;
import maestro.svg.SVGInstance;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Artyom on 7/6/2015.
 */
public class MusicImageMaker extends MIMDefaultMaker {

    public static final String TAG = MusicImageMaker.class.getSimpleName();

    @Override
    public Bitmap getBitmap(ImageLoadObject loadObject, Context ctx) {
        Bitmap bitmap = null;
        if (loadObject.getObject() instanceof Album) {
            bitmap = getBitmap(loadObject, ((Album) loadObject.getObject()).artPath, false);
        } else if (loadObject.getObject() instanceof Song) {
            Song song = (Song) loadObject.getObject();
            Album album = MediaStoreHelper.getAlbum(ctx, song.ParentId);
            if (album != null)
                bitmap = getBitmap(loadObject, album.artPath, false);
            if (bitmap == null) {
                Object object = LastFmApi.getTrackInfo(LastFmApi.buildTrackInfoRequest(song.Title, song.Author), null, false);
                if (object instanceof LastFmTrack) {
                    LastFmTrack track = (LastFmTrack) object;
                    if (track.getAlbum() != null && track.getAlbum().getImages() != null && track.getAlbum().getImages().length > 0)
                        return getBitmap(loadObject, track.getAlbum().getImages()[track.getAlbum().getImages().length - 1].getUrl(), true);
                }
            }
        } else if (loadObject.getObject() instanceof Genre) {

        } else if (loadObject.getObject() instanceof Artist) {
            Object object = LastFmApi.getArtistInfo(LastFmApi.buildArtistInfoRequest(((Artist) loadObject.getObject()).Title), null, false);
            if (object instanceof LastFmArtist) {
                LastFmArtist artist = (LastFmArtist) object;
                if (artist.getImages() != null && artist.getImages().length > 0)
                    return getBitmap(loadObject, artist.getImages()[artist.getImages().length - 1].getUrl(), true);
            }
        }
        if (bitmap != null)
            return bitmap;
        bitmap = getSvg(loadObject, loadObject.getObject());
        if (bitmap != null)
            return bitmap;
        return super.getBitmap(loadObject, ctx);
    }

    private final Bitmap getBitmap(ImageLoadObject loadObject, String path, boolean network) {
        InputStream inputStream;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = loadObject.getConfig();
        options.inJustDecodeBounds = true;
        inputStream = getStream(loadObject, path, network);
        if (inputStream == null)
            return null;
        BitmapFactory.decodeStream(inputStream, null, options);
        options.inSampleSize = calculateInSampleSize(options, loadObject.getWidth(), loadObject.getHeight());
        options.inJustDecodeBounds = false;
        inputStream = getStream(loadObject, path, network);
        if (inputStream == null)
            return null;
        return BitmapFactory.decodeStream(inputStream, null, options);
    }

    private final InputStream getStream(ImageLoadObject loadObject, String path, boolean network) {
        try {
            if (path == null)
                return null;
            if (!network) {
                return new FileInputStream(path);
            } else {
//                HttpClient client = new DefaultHttpClient();
//                HttpGet get = new HttpGet(path);
//                HttpResponse response =  client.execute(get);
//                return response.getEntity().getContent();
                HttpURLConnection conn = (HttpURLConnection) new URL(path).openConnection();
                conn.setConnectTimeout(10000);
                conn.setReadTimeout(10000);
                return conn.getInputStream();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private Bitmap getSvg(ImageLoadObject loadObject, Object object) {
        loadObject.diskCache(false);
        if (object instanceof Album) {
            return SVGInstance.getDrawable(R.raw.ic_album, Color.WHITE, loadObject.getWidth() / 24f).getBitmap();
        } else if (object instanceof Song) {
            return SVGInstance.getDrawable(R.raw.ic_audiotrack, Color.parseColor("#EDEDED"), loadObject.getWidth() / 24f).getBitmap();
        }
        return null;
    }

}
