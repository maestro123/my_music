package maestro.my.music.data.radio;

import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Artyom on 7/19/2015.
 */
public class RadioRequest {

    public static final String TAG = RadioRequest.class.getSimpleName();

    private HttpClient mClient;
    private String mUrl;
    private Object mResult;
    private IObjectPrepare mIObjectPrepare;

    public interface IObjectPrepare {
        Object makeObject(Object object);
    }

    public RadioRequest(String url, IObjectPrepare objectPrepare) {
        mUrl = url;
        mIObjectPrepare = objectPrepare;
    }

    private static final HttpClient getClient() {
        return new DefaultHttpClient();
    }

    public void process() {
        mClient = getClient();
        HttpGet get = new HttpGet(mUrl);
        try {
            HttpResponse response = mClient.execute(get);
            final int statusCode = response.getStatusLine().getStatusCode();
            Log.e(TAG, "statusCode: " + statusCode);
            if (statusCode == 200) {
                String result = EntityUtils.toString(response.getEntity());
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    mResult = mIObjectPrepare.makeObject(jsonObject);
                }catch (JSONException e){
                    try {
                        JSONArray array = new JSONArray(result);
                        mResult = mIObjectPrepare.makeObject(array);
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                }
            } else {
                //TODO: process error by status code
            }
        } catch (IOException e) {
            e.printStackTrace();
            //TODO: error in
        }
    }

    public Object getResult() {
        return mResult;
    }

    public void cancel() {
        if (mClient != null) {
            mClient.getConnectionManager().shutdown();
        }
    }

}
