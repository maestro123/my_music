package maestro.my.music.data.radio;

import org.json.JSONArray;

/**
 * Created by Artyom on 7/19/2015.
 */
public class RadioCategoriesList {

    private JSONArray mArray;
    private RadioCategory[] mStations;

    public RadioCategoriesList(JSONArray jsonObject) {
        mArray = jsonObject;
        prepare();
    }

    public void prepare() {
        final int size = mArray.length();
        if (size > 0) {
            mStations = new RadioCategory[size];
            for (int i = 0; i < size; i++) {
                mStations[i] = new RadioCategory(mArray.optJSONObject(i));
            }
        }
    }

    public int getSize() {
        return mStations != null ? mStations.length : 0;
    }

    public RadioCategory getStationAt(int position) {
        return mStations[position];
    }

}
