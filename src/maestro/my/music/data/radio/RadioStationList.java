package maestro.my.music.data.radio;

import org.json.JSONArray;

/**
 * Created by Artyom on 7/19/2015.
 */
public class RadioStationList {

    private JSONArray mArray;
    private RadioStation[] mStations;

    public RadioStationList(JSONArray jsonObject) {
        mArray = jsonObject;
        prepare();
    }

    public void prepare() {
        final int size = mArray.length();
        if (size > 0) {
            mStations = new RadioStation[size];
            for (int i = 0; i < size; i++) {
                mStations[i] = new RadioStation(mArray.optJSONObject(i));
            }
        }
    }

    public int getSize() {
        return mStations != null ? mStations.length : 0;
    }

    public RadioStation getStationAt(int position) {
        return mStations[position];
    }

}
