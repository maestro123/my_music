package maestro.my.music.data.radio;

import android.graphics.BitmapFactory;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;

/**
 * Created by Artyom on 7/19/2015.
 */
public class RadioStation extends BaseRadioObject implements Parcelable {

    private String mImageUrl;
    private RadioStream[] mStreams;
    private RadioCategory[] mCategories;
    private int imageWidth;
    private int imageHeight;

    public RadioStation(JSONObject jsonObject) {
        super(jsonObject);
    }

    public RadioStation(Parcel source){
        super(source);
        try {
            jsonObject = new JSONObject(source.readString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        imageWidth = source.readInt();
        imageHeight = source.readInt();
        prepare();
    }

    @Override
    public void prepare() {
        super.prepare();
        if (jsonObject.has(DirbleAPI.PARAM_IMAGE)) {
            mImageUrl = jsonObject.optJSONObject(DirbleAPI.PARAM_IMAGE).optString(DirbleAPI.PARAM_URL);
        }

        if (Looper.myLooper() != Looper.getMainLooper() && imageWidth == 0 && imageHeight == 0
                &&!TextUtils.isEmpty(mImageUrl) && !mImageUrl.equals("null")) {
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(new URL(getImageUrl()).openStream(), null, options);
                imageWidth = options.outWidth;
                imageHeight = options.outHeight;
                Log.e("ImageTest", "w: " + imageWidth + ", h: " + imageHeight);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("TEST", "image url: " + mImageUrl);
            }
        }

        if (jsonObject.has(DirbleAPI.PARAM_STREAMS)) {
            JSONArray array = jsonObject.optJSONArray(DirbleAPI.PARAM_STREAMS);
            final int size = array.length();
            if (size > 0) {
                mStreams = new RadioStream[size];
                for (int i = 0; i < size; i++) {
                    mStreams[i] = new RadioStream(array.optJSONObject(i));
                }
            }
        }
        if (jsonObject.has(DirbleAPI.PARAM_CATEGORIES)) {
            JSONArray array = jsonObject.optJSONArray(DirbleAPI.PARAM_CATEGORIES);
            final int size = array.length();
            if (size > 0) {
                mCategories = new RadioCategory[size];
                for (int i = 0; i < size; i++) {
                    mCategories[i] = new RadioCategory(array.optJSONObject(i));
                }
            }
        }
    }

    public String getName() {
        return jsonObject.optString(DirbleAPI.PARAM_NAME);
    }

    public String getCountry() {
        return jsonObject.optString(DirbleAPI.PARAM_COUNTRY);
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public String getWebSite() {
        return jsonObject.optString(DirbleAPI.PARAM_WEBSITE);
    }

    public String getCreatedTime() {
        return jsonObject.optString(DirbleAPI.PARAM_CREATED_AT);
    }

    public String getUpdatedTime() {
        return jsonObject.optString(DirbleAPI.PARAM_UPDATED_AT);
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public RadioCategory[] getCategories() {
        return mCategories;
    }

    public RadioStream[] getStreams() {
        return mStreams;
    }

    public RadioStream getHighestStream() {
        if (mStreams != null && mStreams.length > 0) {
            RadioStream stream = null;
            for (RadioStream st : mStreams) {
                if (stream == null) {
                    stream = st;
                } else if (stream.getBitrate() < st.getBitrate()) {
                    stream = st;
                }
            }
            return stream;
        }
        return null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(jsonObject.toString());
        dest.writeInt(imageWidth);
        dest.writeInt(imageHeight);
    }

    public static final Creator<RadioStation> CREATOR = new Creator<RadioStation>() {
        @Override
        public RadioStation createFromParcel(Parcel source) {
            return new RadioStation(source);
        }

        @Override
        public RadioStation[] newArray(int size) {
            return new RadioStation[size];
        }
    };

}