package maestro.my.music.data.radio;

import android.os.Parcel;
import org.json.JSONObject;

/**
 * Created by Artyom on 7/19/2015.
 */
public class BaseRadioObject {

    JSONObject jsonObject;

    public BaseRadioObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
        prepare();
    }

    public BaseRadioObject(Parcel source){}

    public void prepare() {
    }

    public int getId() {
        return jsonObject.optInt(DirbleAPI.PARAM_ID);
    }

    public String getDescription() {
        return jsonObject.optString(DirbleAPI.PARAM_DESCRIPTION);
    }

    public String getSlug() {
        return jsonObject.optString(DirbleAPI.PARAM_SLUG);
    }

}
