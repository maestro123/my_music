package maestro.my.music.data.radio;

import org.json.JSONObject;

/**
 * Created by Artyom on 7/19/2015.
 */
public class RadioStream extends BaseRadioObject {

    public RadioStream(JSONObject jsonObject) {
        super(jsonObject);
    }

    public String getUrl() {
        return jsonObject.optString(DirbleAPI.PARAM_STREAM);
    }

    public int getBitrate() {
        return jsonObject.optInt(DirbleAPI.PARAM_BITRATE);
    }

    public String getContentType() {
        return jsonObject.optString(DirbleAPI.PARAM_CONTENT_TYPE);
    }

    public String getStatus(){
        return jsonObject.optString(DirbleAPI.PARAM_STATUS);
    }

}
