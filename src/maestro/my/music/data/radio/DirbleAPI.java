package maestro.my.music.data.radio;

import org.json.JSONArray;

/**
 * Created by Artyom on 7/19/2015.
 */
public class DirbleAPI {

    private static final String APP_TOKEN = "?token=ceb3c55c28e42625b88db7f677";

    private static final String BASE_URL = "http://api.dirble.com/v2";

    private static final String CATEGORIES = BASE_URL + "/categories";
    private static final String CATEGORY_STATIONS = BASE_URL + "/category/%s/stations";
    private static final String PRIMARY_STATIONS = BASE_URL + "/stations";

    public static final String PARAM_ID = "id";
    public static final String PARAM_TITLE = "title";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_DESCRIPTION = "description";
    public static final String PARAM_SLUG = "slug";
    public static final String PARAM_ANCESTRY = "ancestry";
    public static final String PARAM_STREAM = "stream";
    public static final String PARAM_STREAMS = "streams";
    public static final String PARAM_BITRATE = "bitrate";
    public static final String PARAM_CONTENT_TYPE = "content_type";
    public static final String PARAM_STATUS = "status";
    public static final String PARAM_COUNTRY = "country";
    public static final String PARAM_WEBSITE = "website";
    public static final String PARAM_CREATED_AT = "created_at";
    public static final String PARAM_UPDATED_AT = "updated_at";
    public static final String PARAM_IMAGE = "image";
    public static final String PARAM_URL = "url";
    public static final String PARAM_THUMB = "thumb";
    public static final String PARAM_CATEGORIES = "categories";

    public static final RadioRequest getAllCategories() {
        return new RadioRequest(CATEGORIES + APP_TOKEN, new RadioRequest.IObjectPrepare() {
            @Override
            public Object makeObject(Object object) {
                return new RadioCategoriesList((JSONArray) object);
            }
        });
    }

    public static final RadioRequest getCategoryStations(int id) {
        return new RadioRequest(String.format(CATEGORY_STATIONS + APP_TOKEN, id), new RadioRequest.IObjectPrepare() {
            @Override
            public Object makeObject(Object object) {
                return new RadioStationList((JSONArray) object);
            }
        });
    }

    public static final RadioRequest getStations() {
        return new RadioRequest(PRIMARY_STATIONS + APP_TOKEN, new RadioRequest.IObjectPrepare() {
            @Override
            public Object makeObject(Object object) {
                return new RadioStationList((JSONArray) object);
            }
        });
    }


}
