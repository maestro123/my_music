package maestro.my.music.data.radio;

import org.json.JSONObject;

/**
 * Created by Artyom on 7/19/2015.
 */
public class RadioCategory extends BaseRadioObject{

    public RadioCategory(JSONObject jsonObject) {
        super(jsonObject);
    }

    public String getTitle() {
        return jsonObject.optString(DirbleAPI.PARAM_TITLE);
    }

    public String getDescription() {
        return jsonObject.optString(DirbleAPI.PARAM_DESCRIPTION);
    }

    public String getAncestry() {
        return jsonObject.optString(DirbleAPI.PARAM_ANCESTRY);
    }

}
