package maestro.my.music.data;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.audiofx.Visualizer;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import com.dream.android.mim.ImageLoadObject;
import com.dream.android.mim.MIM;
import com.dream.android.mim.MIMCirclePostMaker;
import com.msoft.android.mplayer.lib.models.Song;
import maestro.my.music.R;
import maestro.my.music.Utils;
import maestro.my.music.data.radio.RadioStation;
import maestro.my.music.utils.E;
import maestro.my.music.utils.IcyStreamMetadataRetriever;
import maestro.my.music.utils.IcyStreamMetadataRetriever.OnMetadataUpdateListener;
import maestro.svg.SVGInstance;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Artyom on 7/6/2015.
 */
public class MyMusicService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener,
        MediaPlayer.OnErrorListener, MediaPlayer.OnBufferingUpdateListener, OnMetadataUpdateListener {

    public static final String TAG = MyMusicService.class.getSimpleName();

    public static final String ACTION_CLOSE = "maestro.my.music.playback.CLOSE";
    public static final String ACTION_TOGGLE = "maestro.my.music.playback.TOGGLE";
    public static final String ACTION_NEXT = "maestro.my.music.playback.NEXT";

    private static final String PLAY_BITMAP = "play_bitmap";
    private static final String PAUSE_BITMAP = "pause_bitmap";
    private static final String NEXT_BITMAP = "pause_bitmap";
    private static final String CLOSE_BITMAP = "close_bitmap";

    private static final int NOTIFICATION_ID = 0;

    public enum PLAY_STATE {
        IDLE, PLAYING, STOP, SONG_CHANGE, BUFFERING
    }

    public enum LOOP_STATE {
        NO_LOOP, ALL_LOOP, SINGLE_LOOP
    }

    private MediaPlayer mPlayer;
    private Visualizer mVisualizer;
    private RadioStation mRadioStation;
    private List<Song> mSongs;
    private PLAY_STATE mState = PLAY_STATE.IDLE;
    private LOOP_STATE mLoopState = LOOP_STATE.ALL_LOOP;
    private HashMap<String, MyMusicServiceListener> mListeners = new HashMap<>();
    private HashMap<String, MyMusicServiceUpdateListener> mUpdateListeners = new HashMap<>();
    private HashMap<String, MyMusicServiceVisualizerListener> mVisualizerListeners = new HashMap<>();
    private HashMap<String, Bitmap> mBitmapCache = new HashMap<>();
    private NotificationManager mNotificationManager;
    private Thread mUpdateThread;
    private ImageLoadObject mLoadObject;
    private IcyStreamMetadataRetriever mRadioInfoRetriever;
    private boolean isRequiredRelease;
    private boolean isShuffle;
    private boolean isActualPlaying;
    private int mPlayingPosition;
    private int mCoverSize;

    @Override
    public void onCreate() {
        super.onCreate();
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mPlayer = new MediaPlayer();
        mPlayer.setOnBufferingUpdateListener(this);
        mPlayer.setOnPreparedListener(this);
        mPlayer.setOnCompletionListener(this);
        mPlayer.setOnErrorListener(this);
        mRadioInfoRetriever = new IcyStreamMetadataRetriever();
        mRadioInfoRetriever.addOnMetadataUpdateListener(this);
        mVisualizer = new Visualizer(mPlayer.getAudioSessionId());
        mVisualizer.setCaptureSize(64);
        mVisualizer.setDataCaptureListener(new Visualizer.OnDataCaptureListener() {
            @Override
            public void onWaveFormDataCapture(Visualizer visualizer, byte[] waveform, int samplingRate) {
                synchronized (mVisualizerListeners) {
                    for (MyMusicServiceVisualizerListener listener : mVisualizerListeners.values()) {
                        try {
                            if (listener != null)
                                listener.onWaveFormDataCapture(waveform, samplingRate);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFftDataCapture(Visualizer visualizer, byte[] fft, int samplingRate) {

            }
        }, Visualizer.getMaxCaptureRate(), true, false);
        mVisualizer.setEnabled(true);
        mCoverSize = getResources().getDimensionPixelSize(R.dimen.notification_cover_size);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && !TextUtils.isEmpty(intent.getAction())) {
            try {
                if (intent.getAction().equals(ACTION_CLOSE)) {
                    stop();
                } else if (intent.getAction().equals(ACTION_TOGGLE)) {
                    binder.toggle();
                } else if (intent.getAction().equals(ACTION_NEXT)) {
                    binder.next();
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        synchronized (mBitmapCache) {
            for (Bitmap bitmap : mBitmapCache.values()) {
                bitmap.recycle();
            }
            mBitmapCache.clear();
        }
        if (mLoadObject != null) {
            mLoadObject.cancel();
        }
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        E.e(TAG, "onBufferUpdate: " + percent);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        E.e(TAG, "onCompletion");
        //TODO: process to next
        if (mSongs != null) {
            Log.e(TAG, "mLoopState: " + mLoopState);
            switch (mLoopState) {
                case ALL_LOOP:
                    try {
                        binder.next();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    break;
                case NO_LOOP:
                    if (mPlayingPosition != mSongs.size() - 1) {
                        mPlayingPosition++;
                        playCurrent();
                    }
                    break;
                case SINGLE_LOOP:
                    playCurrent();
                    break;
            }
        }

    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        E.e(TAG, "onError: " + what + ", " + extra);
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        E.e(TAG, "onPrepared");
        try {
            changeState(PLAY_STATE.SONG_CHANGE);
            binder.play();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMetadataUpdate(Map<String, String> metadata) {
        updateNotification();
        synchronized (mListeners) {
            for (MyMusicServiceUpdateListener listener : mUpdateListeners.values()) {
                try {
                    if (listener != null)
                        listener.onRadioInfoUpdate(metadata);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private final void changeState(PLAY_STATE state) {
        Log.e(TAG, "changeState: " + state);
        mState = state;
        updateNotification();
        synchronized (mListeners) {
            for (MyMusicServiceListener listener : mListeners.values()) {
                try {
                    if (listener != null)
                        listener.onPlayStateChange(state.ordinal());
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void addToQueue(Object object) {
        //TODO: add playing view
    }

    private final void stop() {
        try {
            binder.pause();
            isActualPlaying = false;
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        removeNotification();
    }

    private final IMyMusicService.Stub binder = new IMyMusicService.Stub() {

        @Override
        public void play() throws RemoteException {
            if (mPlayer != null) {
                mPlayer.start();
                if (mRadioStation != null) {
                    mRadioInfoRetriever.startRetrieve(mRadioStation.getStreams()[mPlayingPosition].getUrl());
                }
                isActualPlaying = true;
                changeState(PLAY_STATE.PLAYING);
            }
        }

        @Override
        public void pause() throws RemoteException {
            if (mPlayer != null) {
                mPlayer.pause();
                mRadioInfoRetriever.stop();
                isActualPlaying = false;
                changeState(PLAY_STATE.STOP);
            }
        }

        @Override
        public void toggle() throws RemoteException {
            if (isPlaying()) {
                pause();
            } else {
                play();
            }
        }

        @Override
        public void prev() throws RemoteException {
            if (mSongs != null) {
                if (mPlayingPosition > 0) {
                    mPlayingPosition--;
                } else {
                    mPlayingPosition = mSongs.size() - 1;
                }
                playCurrent();
            }
        }

        @Override
        public void next() throws RemoteException {
            if (mSongs != null) {
                if (mPlayingPosition < mSongs.size() - 1) {
                    mPlayingPosition++;
                } else {
                    mPlayingPosition = 0;
                }
                playCurrent();
            }
        }

        @Override
        public boolean isPlaying() throws RemoteException {
            return mPlayer != null && mPlayer.isPlaying();
        }

        @Override
        public boolean isPlayingSongs() {
            return mRadioStation == null;
        }

        @Override
        public List<Song> getCurrentPlayingSongs() {
            return mSongs;
        }

        @Override
        public Song getCurrentPlayingSong() throws RemoteException {
            return mSongs != null ? mSongs.get(mPlayingPosition) : null;
        }

        @Override
        public int getCurrentPlayingPosition() {
            return mPlayingPosition;
        }

        @Override
        public void setRadioData(RadioStation station, int position) {
            mSongs = null;
            mRadioStation = station;
            mPlayingPosition = position;
            playCurrent();
        }

        @Override
        public void setSongsData(List<Song> songs, int position) {
            mRadioStation = null;
            mSongs = songs;
            mPlayingPosition = position;
            playCurrent();
        }

        @Override
        public void attachListener(String tag, MyMusicServiceListener listener) {
            synchronized (mListeners) {
                mListeners.put(tag, listener);
            }
        }

        @Override
        public void detachListener(String tag) {
            synchronized (mListeners) {
                mListeners.remove(tag);
            }
        }

        @Override
        public void attachVisualizerListener(String tag, MyMusicServiceVisualizerListener listener) throws RemoteException {
            synchronized (mVisualizerListeners) {
                mVisualizerListeners.put(tag, listener);
            }
        }

        @Override
        public void detachVisualizerListener(String tag) throws RemoteException {
            synchronized (mVisualizerListeners) {
                mVisualizerListeners.remove(tag);
            }
        }

        @Override
        public void attachUpdateListener(String tag, MyMusicServiceUpdateListener listener) throws RemoteException {
            synchronized (mUpdateListeners) {
                mUpdateListeners.put(tag, listener);
            }
        }

        @Override
        public void detachUpdateListener(String tag) throws RemoteException {
            synchronized (mUpdateListeners) {
                mUpdateListeners.remove(tag);
            }
        }

        @Override
        public RadioStation getCurrentRadioStation() {
            return mRadioStation;
        }

        @Override
        public int getPlayingSessionId() {
            return mPlayer != null ? mPlayer.getAudioSessionId() : -1;
        }

        @Override
        public boolean isTrackNavigationEnable() throws RemoteException {
            return mRadioStation == null;
        }

        @Override
        public boolean isShuffle() {
            return isShuffle;
        }

        @Override
        public int getLoopState() {
            return mLoopState.ordinal();
        }

        @Override
        public void setShuffle(boolean shuffle) {
            isShuffle = shuffle;
        }

        @Override
        public void setLoopState(int state) {
            mLoopState = LOOP_STATE.values()[state];
        }

        @Override
        public void seekTo(int progress) throws RemoteException {
            if (mPlayer != null) {
                mPlayer.seekTo(progress);
                if (mPlayer.isPlaying()) {
                    play();
                }
            }
        }

    };

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    private void playCurrent() {
        startProgressNotifyThread();
        if (isActualPlaying) {
            mPlayer.pause();
            mPlayer.reset();
            mRadioInfoRetriever.stop();
        }
        try {
            if (mRadioStation != null) {
                final String url = mRadioStation.getStreams()[mPlayingPosition].getUrl();
                mPlayer.setDataSource(mRadioStation.getStreams()[mPlayingPosition].getUrl());
                mRadioInfoRetriever.restart(url);
                changeState(PLAY_STATE.BUFFERING);
            } else {
                mPlayer.setDataSource(mSongs.get(mPlayingPosition).Path);
            }
            isRequiredRelease = true;
            mPlayer.prepareAsync();
            if (mSongs != null) {
                Database.Instance.addToMostPlayed(binder.getCurrentPlayingSong());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private final void startProgressNotifyThread() {
        if (mUpdateThread == null || mUpdateThread.isInterrupted()) {
            mUpdateThread = new Thread() {
                @Override
                public void run() {
                    super.run();
                    while (!isInterrupted()) {
                        if (mPlayer != null && mPlayer.isPlaying()) {
                            synchronized (mUpdateListeners) {
                                for (MyMusicServiceUpdateListener listener : mUpdateListeners.values()) {
                                    if (listener != null)
                                        try {
                                            listener.onPlayTimeUpdate(mPlayer.getCurrentPosition(), mPlayer.getDuration());
                                        } catch (RemoteException e) {
                                            e.printStackTrace();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                }
                            }
                        }
                        try {
                            sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            };
            mUpdateThread.start();
        }
    }

    private final void stopProgressNotifyThread() {
        if (mUpdateThread != null) {
            mUpdateThread.interrupt();
            mUpdateThread = null;
        }
    }

    private final Object getCurrentPlayObject() {
        return mRadioStation != null ? mRadioStation : mSongs.get(mPlayingPosition);
    }

    public final void updateNotification() {
        RemoteViews mRemoteViews = new RemoteViews(getPackageName(), R.layout.notification_view_small);
        try {
            mRemoteViews.setImageViewBitmap(R.id.toggle, getSVGBitmap(binder.isPlaying() ? PAUSE_BITMAP : PLAY_BITMAP,
                    binder.isPlaying() ? R.raw.ic_pause : R.raw.ic_play));
            if (!binder.isTrackNavigationEnable()) {
                mRemoteViews.setViewVisibility(R.id.next, View.GONE);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        Object object = getCurrentPlayObject();
        mRemoteViews.setImageViewBitmap(R.id.next, getSVGBitmap(NEXT_BITMAP, R.raw.ic_fast_forward));
        mRemoteViews.setImageViewBitmap(R.id.close, getSVGBitmap(CLOSE_BITMAP, R.raw.ic_close));
        String title = null;
        String author = null;

        if (mRadioStation != null) {
            if (mState == PLAY_STATE.BUFFERING) {
                author = getString(R.string.buffering);
            } else {
                title = mRadioInfoRetriever.getTitle();
                author = mRadioInfoRetriever.getArtist();
                if (TextUtils.isEmpty(author) && !TextUtils.isEmpty(title)) {
                    author = title;
                    title = null;
                }
            }
        }
        if (TextUtils.isEmpty(title)) {
            title = Utils.getTitle(object);
        }
        if (TextUtils.isEmpty(author)) {
            author = Utils.getAdditional(getApplicationContext(), object);
        }

        mRemoteViews.setTextViewText(R.id.title, title);
        mRemoteViews.setTextViewText(R.id.additional, author);

        mRemoteViews.setOnClickPendingIntent(R.id.close, getServiceIntent(ACTION_CLOSE));
        mRemoteViews.setOnClickPendingIntent(R.id.toggle, getServiceIntent(ACTION_TOGGLE));
        mRemoteViews.setOnClickPendingIntent(R.id.next, getServiceIntent(ACTION_NEXT));

        String key = Utils.getKey(object) + "_notification";
        Bitmap bitmap = getBitmap(key);
        if (bitmap == null) {
            if (mLoadObject != null && !mLoadObject.getKey().equals(key)) {
                mLoadObject.cancel();
                removeBitmap(mLoadObject.getKey());
            }
            if (object instanceof RadioStation) {
                mLoadObject = MIM.by(Utils.MIM_NETWORK).of(key, ((RadioStation) object).getImageUrl()).object(object);
            } else {
                mLoadObject = MIM.by(Utils.MIM_IMAGE).of(key).object(object).postMaker(new MIMCirclePostMaker());
            }
            mLoadObject.size(mCoverSize, mCoverSize)
                    .listener(new ImageLoadObject.OnImageLoadEventListener() {
                        @Override
                        public void onImageLoadEvent(IMAGE_LOAD_EVENT event, ImageLoadObject loadObject) {
                            if (event == IMAGE_LOAD_EVENT.FINISH) {
                                if (loadObject.getResultBitmap() != null) {
                                    mBitmapCache.put(loadObject.getKey(), loadObject.getResultBitmap());
                                    updateNotification();
                                }
                            }
                        }
                    }).useRecyclerDrawable(false).async();
        } else {
            mRemoteViews.setImageViewBitmap(R.id.image, bitmap);
        }
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setContent(mRemoteViews)
                        .setSmallIcon(R.drawable.ic_launcher);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    public final void removeNotification() {
        mNotificationManager.cancel(NOTIFICATION_ID);
    }

    private final Bitmap getBitmap(String key) {
        synchronized (mBitmapCache) {
            return mBitmapCache.get(key);
        }
    }

    private final void addBitmap(String key, Bitmap bitmap) {
        synchronized (mBitmapCache) {
            mBitmapCache.put(key, bitmap);
        }
    }

    private final void removeBitmap(String key) {
        synchronized (mBitmapCache) {
            Bitmap object = mBitmapCache.remove(key);
            if (object != null) {
                object.recycle();
            }
        }
    }

    private final Bitmap getSVGBitmap(String key, int id) {
        Bitmap bitmap = getBitmap(key);
        if (bitmap == null) {
            bitmap = SVGInstance.getDrawable(id, Color.WHITE).getBitmap();
            addBitmap(key, bitmap);
        }
        return bitmap;
    }

    private PendingIntent getServiceIntent(String action) {
        Intent intent = new Intent(this, MyMusicService.class);
        intent.setAction(action);
        return PendingIntent.getService(this, 0, intent, 0);
    }

}