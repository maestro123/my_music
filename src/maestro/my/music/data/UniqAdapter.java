package maestro.my.music.data;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.dream.android.mim.ImageLoadObject;
import com.dream.android.mim.MIM;
import com.dream.android.mim.MIMCirclePostMaker;
import com.dream.android.mim.RecyclingImageView;
import com.msoft.android.helpers.MFormatter;
import com.msoft.android.mplayer.lib.models.Genre;
import com.msoft.android.mplayer.lib.models.Playlist;
import com.msoft.android.mplayer.lib.models.Song;
import maestro.my.music.MyMusicApplication;
import maestro.my.music.R;
import maestro.my.music.Utils;
import maestro.my.music.ui.WaveIndicator;
import maestro.my.music.utils.ThemeHolder;
import maestro.svg.SVGInstance;

import java.util.ArrayList;

/**
 * Created by Artyom on 7/6/2015.
 */
public class UniqAdapter extends RecyclerView.Adapter<UniqAdapter.Holder> implements View.OnClickListener, View.OnLongClickListener {

    public static final String TAG = UniqAdapter.class.getSimpleName();

    private static final Handler mHandler = new Handler(Looper.getMainLooper());

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<Object> mItems;
    private Song mCurrentPlaySong;
    private TYPE mType;
    private MIMCirclePostMaker mPostMaker;
    private OnItemClickListener mClickListener;
    private OnItemLongClickListener mLongClickListener;
    private int imageWidth, imageHeight;
    private int mSpanCount;

    private boolean isShowIcon = true;
    private boolean isShowTime = false;

    public enum TYPE {
        GRID, SMALL_GRID, ROW, BIG_ROW
    }

    public interface OnItemClickListener<T> {
        public void onItemClick(Holder holder, T item);
    }

    public interface OnItemLongClickListener<T> {
        public boolean onItemLongClick(Holder holder, T item);
    }

    public UniqAdapter(Context context, TYPE type, int spanCount) {
        mContext = context;
        mType = type;
        mSpanCount = spanCount;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final Resources resources = context.getResources();
        final boolean isTablet = resources.getBoolean(R.bool.is7inch) || resources.getBoolean(R.bool.is10inch);
        final boolean isLandscape = resources.getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        imageWidth = imageHeight = (int) ((resources.getDisplayMetrics().widthPixels * (isTablet && isLandscape ? 0.65f : 1f)) / spanCount);
        mPostMaker = new MIMCirclePostMaker(context.getResources().getDimensionPixelSize(R.dimen.circle_stroke_width), "#9E9E9E");
        try {
            mCurrentPlaySong = getService().getCurrentPlayingSong();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void update(ArrayList<Object> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener mClickListener) {
        this.mClickListener = mClickListener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener mLongClickListener) {
        this.mLongClickListener = mLongClickListener;
    }

    public void setIsShowIcon(boolean isShowIcon) {
        this.isShowIcon = isShowIcon;
    }

    public void setIsShowTime(boolean isShowTime) {
        this.isShowTime = isShowTime;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Holder holder = new Holder(mInflater.inflate(mType == TYPE.GRID ? R.layout.grid_item_view
                : mType == TYPE.SMALL_GRID
                ? R.layout.small_grid_item_view
                : R.layout.row_item_view, null));
        holder.itemView.setOnClickListener(this);
        holder.itemView.setOnLongClickListener(this);
        return holder;
    }

    @Override
    public void onBindViewHolder(Holder holder, int i) {
        Object item = mItems.get(i);
        if (isShowIcon) {
            if (mType == TYPE.GRID) {
                holder.Image.getLayoutParams().width = imageWidth;
                holder.Image.getLayoutParams().height = imageHeight;
            }
            if (item instanceof Genre) {
                SVGInstance.applySVG(holder.Image, R.raw.genres, Color.parseColor("#757575"));
                holder.Image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            } else if (item instanceof Playlist) {
                SVGInstance.applySVG(holder.Image, R.raw.playlist);
            } else {
                ImageLoadObject loadObject = MIM.by(Utils.MIM_IMAGE).to(holder.Image, Utils.getKey(item) + mType).object(item);
                if (mType == TYPE.ROW) {
                    loadObject.postMaker(mPostMaker);
                }
                holder.Image.setLoadObject(loadObject);
            }
        }
        holder.Title.setText(Utils.getTitle(item));
        String additional = Utils.getAdditional(mContext, item);
        if (!TextUtils.isEmpty(additional)) {
            holder.Additional.setVisibility(View.VISIBLE);
            holder.Additional.setText(additional);
        } else {
            holder.Additional.setVisibility(View.GONE);
        }
        if (isShowTime && holder.Time != null) {
            holder.Time.setText(MFormatter.formatTimeFromMillis(Utils.getDuration(item)));
        }
        if (holder.Wave != null) {
            if (mCurrentPlaySong != null && mCurrentPlaySong.equals(item)) {
                holder.Wave.setVisibility(View.VISIBLE);
            } else {
                holder.Wave.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mItems != null ? mItems.size() : 0;
    }

    @Override
    public void onClick(View v) {
        Holder holder = (Holder) v.getTag();
        if (holder != null && mClickListener != null) {
            mClickListener.onItemClick(holder, mItems.get(holder.getAdapterPosition()));
        }
    }

    @Override
    public boolean onLongClick(View v) {
        Holder holder = (Holder) v.getTag();
        return holder != null && mLongClickListener != null
                ? mLongClickListener.onItemLongClick(holder, mItems.get(holder.getAdapterPosition())) : false;
    }

    public class Holder extends RecyclerView.ViewHolder {

        RecyclingImageView Image;
        TextView Title;
        TextView Additional;
        TextView Time;
        WaveIndicator Wave;

        public Holder(View itemView) {
            super(itemView);

            Image = (RecyclingImageView) itemView.findViewById(R.id.image);
            Title = (TextView) itemView.findViewById(R.id.title);
            Additional = (TextView) itemView.findViewById(R.id.additional);
            Time = (TextView) itemView.findViewById(R.id.time);
            Wave = (WaveIndicator) itemView.findViewById(R.id.wave_indicator);

            if (!isShowIcon && Image != null) {
                Image.setVisibility(View.GONE);
            }

            if (!isShowTime && Time != null) {
                Time.setVisibility(View.GONE);
            }

            if (Wave != null) {
                Wave.setColor(ThemeHolder.getInstance().getToolbarColor());
                Wave.setPickColor(Color.parseColor("#b401bf"));
                Wave.setDefWidth((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, itemView.getResources().getDisplayMetrics()));
                Wave.setVisibility(View.GONE);
            }
            Image.setHasFixedSize(true);

            itemView.setTag(this);

        }
    }

    private IMyMusicService getService() throws NullPointerException {
        return ((MyMusicApplication) (mContext instanceof Activity
                ? ((Activity) mContext).getApplication() : mContext)).getService();
    }

    private MyMusicServiceListener mServiceListener = new MyMusicServiceListener.Stub() {
        @Override
        public void onPlayStateChange(int state) throws RemoteException {
            MyMusicService.PLAY_STATE mState = MyMusicService.PLAY_STATE.values()[state];
            if (mState == MyMusicService.PLAY_STATE.SONG_CHANGE) {
                mCurrentPlaySong = getService().getCurrentPlayingSong();
                postNotifyDataSetChanged();
            } else {
                //TODO: add release logic
            }
        }
    };

    public MyMusicServiceListener getServiceListener() {
        return mServiceListener;
    }

    public final void postNotifyDataSetChanged(){
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

}
