package maestro.my.music.data;

interface MyMusicServiceListener {

    void onPlayStateChange(in int state);

}