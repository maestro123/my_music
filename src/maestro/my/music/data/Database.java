package maestro.my.music.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;
import com.msoft.android.mplayer.lib.models.Album;
import com.msoft.android.mplayer.lib.models.Playlist;
import com.msoft.android.mplayer.lib.models.Song;
import maestro.my.music.data.radio.RadioStation;

/**
 * Created by Artyom on 8/19/2015.
 */
public class Database extends SQLiteOpenHelper {

    public static final String TAG = Database.class.getSimpleName();
    public static Database Instance;

    private static final String DATABASE_NAME = "mymusic.db";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_RECENT_PLAYED = "recent_played";
    private static final String TABLE_FAVOURITE_RADIO = "fav_radio";
    private static final String TABLE_MOST_PLAYED = "played_items";

    private static final String KEY_ID = "id";
    private static final int COLUMN_ID = 1;

    private static final String KEY_AKEY = "akey";
    private static final int COLUMN_AKEY = COLUMN_ID + 1;

    private static final String KEY_DATA = "data";
    private static final int COLUMN_DATA = COLUMN_ID + 2;

    private static final String KEY_TYPE = "type";
    private static final int COLUMN_TYPE = COLUMN_ID + 3;

    private static final String KEY_COUNT = "count";
    private static final int COLUMN_COUNT = COLUMN_ID + 4;

    private static final String CREATE_DATA_TABLE = "CREATE TABLE %s("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + KEY_AKEY + " TEXT, "
            + KEY_DATA + " TEXT NOT NULL);";

    private static final String CREATE_MOST_PLAYED_TABLE = "CREATE TABLE "
            + TABLE_MOST_PLAYED + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_AKEY + " TEXT,"
            + KEY_TYPE + " INTEGER,"
            + KEY_DATA + " TEXT NOT NULL,"
            + KEY_COUNT + " INTEGER)";

    private static final int TYPE_SONG = 0;
    private static final int TYPE_RADIO = 1;
    private static final int TYPE_ALBUM = 2;
    private static final int TYPE_PLAYLIST = 3;

    private static final String[] aKeySearchTable = new String[]{KEY_AKEY};
    private static final String aKeySearchSelection = KEY_AKEY + "=?";

    private SQLiteDatabase mDatabase;

    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Instance = this;
        mDatabase = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(String.format(CREATE_DATA_TABLE, TABLE_RECENT_PLAYED));
        db.execSQL(String.format(CREATE_DATA_TABLE, TABLE_FAVOURITE_RADIO));
        db.execSQL(CREATE_MOST_PLAYED_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void addToMostPlayed(Object object) {
        String data = null;
        String key = null;
        int type = -1;
        if (object instanceof Song) {
            Song song = (Song) object;
            type = TYPE_SONG;
            key = String.valueOf(song.Id);
            data = key;
        } else if (object instanceof RadioStation) {

        } else if (object instanceof Album) {

        } else if (object instanceof Playlist) {

        }
        if (!TextUtils.isEmpty(key) && !TextUtils.isEmpty(data)) {
            addToMostPlayed(type, key, data);
        }
    }

    public void addToMostPlayed(int type, String key, String data) {
        Cursor cursor = mDatabase.query(TABLE_MOST_PLAYED, aKeySearchTable, aKeySearchSelection,
                new String[]{key}, null, null, null, null);
        if (cursor.getCount() == 0) {
            ContentValues values = new ContentValues();
            values.put(KEY_AKEY, key);
            values.put(KEY_TYPE, type);
            values.put(KEY_DATA, data);
            values.put(KEY_COUNT, 1);
            long id = mDatabase.insert(TABLE_MOST_PLAYED, null, values);
            Log.e(TAG, "addToMostPlayed: id = " + id);
        } else {
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_COUNT, cursor.getInt(COLUMN_COUNT) + 1);
            int count = mDatabase.update(TABLE_MOST_PLAYED, contentValues, aKeySearchSelection, new String[]{key});
            Log.e(TAG, "addToMostPlayed: count = " + count);
        }
        cursor.close();
    }

}