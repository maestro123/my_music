package maestro.my.music.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.dream.android.mim.ImageLoadObject;
import com.dream.android.mim.MIM;
import com.dream.android.mim.MIMColorAnalyzer;
import com.dream.android.mim.RecyclingImageView;
import maestro.my.music.R;
import maestro.my.music.Utils;
import maestro.my.music.ui.TopsLayout;
import maestro.my.music.utils.SpaceItemDecoration;
import maestro.xbox.music.models.BaseXObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Artyom on 7/8/2015.
 */
public class HomeAdapter extends RecyclerView.Adapter {

    public static final String TAG = HomeAdapter.class.getSimpleName();

    public static final int TYPE_RECENT = 0;
    public static final int TYPE_TOPS = 1;

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<Object> mRecent;
    private LinkedHashMap<String, ArrayList> mTops = new LinkedHashMap<>();

    private int mContentWidth;
    private boolean isDisplayRecent = true;

    public HomeAdapter(Context context, int contentWidth) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContentWidth = contentWidth;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        return type == TYPE_RECENT ? new RecentHolder(mInflater.inflate(R.layout.recent_item_view, null)) : type == TYPE_TOPS
                ? new TopsHolder(mInflater.inflate(R.layout.top_item_view, null)) : null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        int viewType = getItemViewType(i);
        switch (viewType) {
            case TYPE_TOPS:
                final TopsHolder holder = (TopsHolder) viewHolder;
                ArrayList items = mTops.get(mTops.keySet().toArray()[getTopPosition(i)]);
                holder.TLayout.addItems(items);
                Object object = items.get(0);
                if (object instanceof BaseXObject) {
                    BaseXObject xObject = (BaseXObject) object;
                    holder.Image.setLoadObject(MIM.by(Utils.MIM_X_NETWORK).to(holder.Image, xObject.getId()).object(xObject).analyze(true).colorAnalyzer(new MIMColorAnalyzer() {
                        @Override
                        public Integer[] analyze(Bitmap bitmap) {
                            Palette palette = Palette.generate(bitmap);
                            if (palette.getVibrantSwatch() != null) {
                                Integer[] vals = new Integer[3];
                                vals[0] = palette.getVibrantSwatch().getRgb();
                                return vals;
                            }
                            return super.analyze(bitmap);
                        }
                    }).listener(new ImageLoadObject.OnImageLoadEventListener() {
                        @Override
                        public void onImageLoadEvent(IMAGE_LOAD_EVENT event, ImageLoadObject loadObject) {
                            if (event == IMAGE_LOAD_EVENT.FINISH) {
                                if (loadObject.getAnalyzedColors() != null){
                                    holder.applyColor(loadObject.getAnalyzedColors()[0]);
                                }
                            }
                        }
                    }));
                }
                break;
            case TYPE_RECENT:
                RecentHolder recentHolder = (RecentHolder) viewHolder;
                recentHolder.mAdapter.update(mRecent);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 && isDisplayRecent ? TYPE_RECENT : TYPE_TOPS;
    }

    public void addTops(String title, ArrayList items) {
        mTops.put(title, items);
        notifyDataSetChanged();
    }

    public void addRecent(ArrayList items) {
        mRecent = items;
        notifyDataSetChanged();
    }

    public int getTopPosition(int position) {
        if (isDisplayRecent) {
            position--;
        }
        return position;
    }

    @Override
    public int getItemCount() {
        int count = getItemsCount();
        if (isDisplayRecent) {
            count++;
        }
        return count;
    }

    public int getItemsCount() {
        return mTops.size();
    }

    public class RecentHolder extends RecyclerView.ViewHolder {

        private RecyclerView List;
        private TextView Title;
        private UniqAdapter mAdapter;

        public RecentHolder(View itemView) {
            super(itemView);
            Title = (TextView) itemView.findViewById(R.id.title);
            List = (RecyclerView) itemView.findViewById(R.id.list);
            List.addItemDecoration(new SpaceItemDecoration(1, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, mContext.getResources().getDisplayMetrics()), 0, true));
            List.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            List.setAdapter(mAdapter = new UniqAdapter(itemView.getContext(), UniqAdapter.TYPE.SMALL_GRID, 1));
        }

    }

    public class TopsHolder extends RecyclerView.ViewHolder {

        private RecyclingImageView Image;
        private TopsLayout TLayout;

        private ColorDrawable TopOverlay;
        private GradientDrawable BottomGradient;

        public TopsHolder(final View itemView) {
            super(itemView);

            Image = (RecyclingImageView) itemView.findViewById(R.id.image);
            TLayout = (TopsLayout) itemView.findViewById(R.id.tops_layout);

            TopOverlay = new ColorDrawable(Color.BLACK);
            TopOverlay.setAlpha((int) (0.45f * 255));
            BottomGradient = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[]{Color.BLACK, Color.TRANSPARENT});

            itemView.findViewById(R.id.tops_image_overlay).setBackgroundDrawable(TopOverlay);
            itemView.findViewById(R.id.tops_shadow_bottom).setBackgroundDrawable(BottomGradient);
            itemView.setBackgroundColor(Color.BLACK);

            TLayout.setDrawer(new TopsLayout.ViewDrawer<BaseXObject>() {
                @Override
                public View getView(BaseXObject item) {
                    View v = View.inflate(itemView.getContext(), R.layout.top_grid_item_view, null);

                    RecyclingImageView Image = (RecyclingImageView) v.findViewById(R.id.image);
                    TextView Title = (TextView) v.findViewById(R.id.title);
                    TextView Additional = (TextView) v.findViewById(R.id.additional);

                    Image.setLoadObject(MIM.by(Utils.MIM_X_NETWORK).to(Image, item.getId()).object(item));
                    Title.setText(item.getName());
                    Additional.setText(item.getSource());

                    return v;
                }
            });

        }

        public void applyColor(int color) {
            TopOverlay.setColor(color);
            TopOverlay.setAlpha((int) (0.45f * 255));
            BottomGradient.setColors(new int[]{color, Color.TRANSPARENT});
            itemView.setBackgroundColor(color);
        }


    }

}
