package maestro.my.music.data;

interface MyMusicServiceVisualizerListener {

    void onWaveFormDataCapture(in byte[] waveform, int samplingRate);

}