package maestro.my.music.data;

/**
 * Created by Artyom on 8/12/2015.
 */
public class SearchItem {

    private String Title;
    private String Duration;
    private String Url;

    public SearchItem(String title, String duration, String url) {
        Title = title;
        Duration = duration;
        Url = url;
    }

    public SearchItem() {

    }

    public void setTitle(String title) {
        Title = title;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getTitle() {
        return Title;
    }

    public String getDuration() {
        return Duration;
    }

    public String getUrl() {
        return Url;
    }

    @Override
    public String toString() {
        return new StringBuilder(getClass().getName()).append(": Title=")
                .append(Title).append(", Duration: ").append(Duration)
                .append(", Url = ").append(Url).toString();
    }

}
