package maestro.my.music.data;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.content.AsyncTaskLoader;
import android.text.TextUtils;
import com.msoft.android.mplayer.lib.MediaStoreHelper;
import com.msoft.android.mplayer.lib.models.*;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Artyom on 7/6/2015.
 */
public class Loders {

    public static class AlbumLoader extends AsyncTaskLoader<ArrayList<Album>> {

        private Long mLoadId;

        public AlbumLoader(Context context, Long loadId) {
            super(context);
            mLoadId = loadId;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public ArrayList<Album> loadInBackground() {
            return MediaStoreHelper.getAlbums(getContext(), mLoadId);
        }
    }

    public static class MusicLoader extends IndexLoader<ArrayList<Song>> {

        private Long mLoadId;

        public MusicLoader(Context context, Long loadId) {
            super(context);
            mLoadId = loadId;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public ArrayList<Song> loadInBackground() {
            ArrayList<Song> songs = MediaStoreHelper.getSongs(getContext(), mLoadId);
            if (songs != null && songs.size() > 0) {
                for (int i = 0; i < songs.size(); i++) {
                    String ch = getFirstChar(songs.get(i).Title);
                    if (ch != null && !dividerPositions.containsValue(ch)) {
                        dividerPositions.put(i, ch);
                    }
                }
            }
            return songs;
        }
    }

    public static class SongsLoader extends AsyncTaskLoader<ArrayList<Song>> {

        private Object mObject;

        public SongsLoader(Context context, Parcelable object) {
            super(context);
            mObject = object;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public ArrayList<Song> loadInBackground() {
            if (mObject instanceof Album) {
                return MediaStoreHelper.getSongs(getContext(), ((Album) mObject).Id);
            } else if (mObject instanceof Artist) {
                return MediaStoreHelper.getSongsByArtist(getContext(), ((Artist) mObject).Id);
            } else if (mObject instanceof Genre) {
                return MediaStoreHelper.getSongsByGenre(getContext(), ((Genre) mObject).Id);
            } else if (mObject instanceof Playlist) {
                return MediaStoreHelper.getSongsByPlaylist(getContext(), ((Playlist) mObject).Id);
            }
            return null;
        }
    }

    public static abstract class IndexLoader<D> extends AsyncTaskLoader<D> {

        HashMap<Integer, String> dividerPositions = new HashMap<>();

        public IndexLoader(Context context) {
            super(context);
        }

        protected String getFirstChar(String string) {
            return !TextUtils.isEmpty(string) ? string.substring(0, 1) : null;
        }

        public final HashMap<Integer, String> getDividers() {
            return dividerPositions;
        }
    }

    public static class ArtistLoader extends IndexLoader<ArrayList<Artist>> {

        private Long mLoadId;

        public ArtistLoader(Context context, Long loadId) {
            super(context);
            mLoadId = loadId;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public ArrayList<Artist> loadInBackground() {
            ArrayList<Artist> artists = MediaStoreHelper.getArtists(getContext());
            if (artists != null && artists.size() > 0) {
                for (int i = 0; i < artists.size(); i++) {
                    String ch = getFirstChar(artists.get(i).Title);
                    if (ch != null && !dividerPositions.containsValue(ch)) {
                        dividerPositions.put(i, ch);
                    }
                }
            }
            return artists;
        }

    }

    public static class GenreLoader extends IndexLoader<ArrayList<Genre>> {

        private Long mLoadId;

        public GenreLoader(Context context, Long loadId) {
            super(context);
            mLoadId = loadId;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public ArrayList<Genre> loadInBackground() {
            ArrayList<Genre> genres = MediaStoreHelper.getGenres(getContext());
            if (genres != null && genres.size() > 0) {
                for (int i = 0; i < genres.size(); i++) {
                    String ch = getFirstChar(genres.get(i).Title);
                    if (ch != null && !dividerPositions.containsValue(ch)) {
                        dividerPositions.put(i, ch);
                    }
                }
            }
            return genres;
        }

    }

    public static class PlaylistLoader extends IndexLoader<ArrayList<Playlist>> {

        private Long mLoadId;

        public PlaylistLoader(Context context, Long loadId) {
            super(context);
            mLoadId = loadId;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public ArrayList<Playlist> loadInBackground() {
            ArrayList<Playlist> artists = MediaStoreHelper.getPlaylists(getContext());
            if (artists != null && artists.size() > 0) {
                for (int i = 0; i < artists.size(); i++) {
                    String ch = getFirstChar(artists.get(i).Title);
                    if (ch != null && !dividerPositions.containsValue(ch)) {
                        dividerPositions.put(i, ch);
                    }
                }
            }
            return artists;
        }

    }

}
