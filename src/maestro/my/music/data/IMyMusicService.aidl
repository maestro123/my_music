package maestro.my.music.data;
import com.msoft.android.mplayer.lib.models.Song;
import maestro.my.music.data.radio.RadioStation;
import maestro.my.music.data.MyMusicServiceListener;
import maestro.my.music.data.MyMusicServiceUpdateListener;
import maestro.my.music.data.MyMusicServiceVisualizerListener;

interface IMyMusicService {

    void play();
    void pause();
    void toggle();
    void prev();
    void next();

    boolean isPlaying();

    boolean isPlayingSongs();

    void setRadioData(in RadioStation playObject, in int position);

    void setSongsData(in List<Song> songs, int position);

    List<Song> getCurrentPlayingSongs();

    Song getCurrentPlayingSong();

    int getCurrentPlayingPosition();

    RadioStation getCurrentRadioStation();

    int getPlayingSessionId();

    boolean isTrackNavigationEnable();

    boolean isShuffle();

    int getLoopState();

    void setShuffle(boolean isShuffle);

    void setLoopState(int state);

    void seekTo(int progress);

    void attachListener(in String tag, in MyMusicServiceListener listener);

    void detachListener(in String tag);

    void attachVisualizerListener(in String tag, in MyMusicServiceVisualizerListener listener);

    void detachVisualizerListener(in String tag);

    void attachUpdateListener(in String tag, in MyMusicServiceUpdateListener listener);

    void detachUpdateListener(in String tag);

}